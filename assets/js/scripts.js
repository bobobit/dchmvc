Carousel = function(){

	var count = 0;
	var index = 0;
	var prev  = 0;
	var curr  = 0;
	var next  = 0;

	return {
		set_count: function(icount){
			count = icount;
		},
		get_count: function(){
			return count;
		},
		set_index: function(iindex){
			index = iindex;
		},
		get_index: function(){
			return index;
		},
		set_prev: function(iprev){
			prev = iprev;
		},
		get_prev: function(){
			return prev;
		},
		set_curr: function(icurr){
			curr = icurr;
		},
		get_curr: function(){
			return curr;
		},
		set_next: function(inext){
			next = inext;
		},
		get_next: function(){
			return next;
		},
	};
};
$(function(){
/*jQuery*/

	var carousel = new Carousel();
	var carouselPhotoGallery = new Carousel();

	/*BLOG SEARCH*/
	$('#blog_search_button').click(function(){
		
		var query = $('#blog_search_query').val();
		var url = $('#blog_search').attr('data-url');

		$('#blog_search').slideUp(function(){
			$.post(url, { blog_search_query: query}, function(data){
				$('#blog_search').html(data);
				$('#blog_search').slideDown();
			});
		});
		
	});

	/*POST SEARCH BY TITLE*/
	$('#post_search_button').click(function(){
		
		var query = $('#post_search_query').val();
		var url = $('#post_search').attr('data-url');

		$('#post_search').slideUp(function(){
			$.post(url, { post_search_query: query}, function(data){
				$('#post_search').html(data);
				$('#post_search').slideDown();
			});
		});
		
	});

	/* BIG PICTURE TOGGLE NAV */	
	var bigpicture_elements = [$('#bigpictureNav'), $('#photo-info')];
	$('#navToggle').click(function (event) {//Nav On/Off
		event.stopPropagation();
		
	    $.each(bigpicture_elements, function(index, element) { 
		    $(element)
				    .toggleClass('hidden-lg hidden-md hidden-sm')
				    .css('display', 'block')
		    ;
		});

	    $('span', this).toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
	    
	});	
	$('#bigpictureNav').mouseleave(function () {//FadeOut koga mousot ke go napusti nav

		if($(document).width() >= 768){//xs < 768, sm >= 768, !Ne prezemaj akcija sa xs extra-small device width
		    $.each(bigpicture_elements, function(index, element) { 
			    $(element).delay( 3500 ).fadeOut(3500, function(){
			    	$(element).addClass('hidden-lg hidden-md hidden-sm');
			    	$('#navToggle>span').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
			    });
			});
		}
	});
	$('#bigpictureNav').click(function () {//Prekini Fadeout so klik na nav

		$.each(bigpicture_elements, function(index, element) { 
		    $(element).stop()
				    .removeClass('hidden-lg hidden-md hidden-sm')
				    .css('opacity', '1')
				    .css('display', 'block')
		    ;
		});

	});
	$('html').click(function (event) {//Nav Off - outside click

		var target = event.target;
		var bul = true;//nav-Off

		/*Ako kliknis na nav ili na nekoj od nejzinite deca togas ne prezemaj akcija, vo sprotivno nav-Off*/
		if (($(target).attr('id') == 'bigpictureNav') || ($(target).parents('nav#bigpictureNav').length>0)) {
			bul = false;
		};

		if (bul) {//nav-off ako klikot e nadvor od nego
	    	$.each(bigpicture_elements, function(index, element) { 
			    $(element)
					    .addClass('hidden-lg hidden-md hidden-sm')
			    ;
			});
			$('#navToggle>span').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        } 
	});
	$( '#photo-info' ).hover(
		function() {//handlerIn
			$.each(bigpicture_elements, function(index, element) { 
			    $(element).stop()
			    		.fadeIn(300)
			    		.css('opacity', '1')
			    		.removeClass( 'hidden-lg hidden-md hidden-sm' )
			    ;
			});
		}, 
		function() {//handlerOut
			$.each(bigpicture_elements, function(index, element) { 
			    $(element).stop()
			    		.fadeOut( 3000 )
			    		.css('opacity', '1')
			    		.delay( 3000, function(){
			    			$(element).stop().addClass( 'hidden-lg hidden-md hidden-sm' );
			    		})
			    		
			    ;
			});
			$('#navToggle>span').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
		}
	);
	/*UPDATE CAROUSEL OBJECT - PREV, CURR, NEXT INDEX*/
	$('#myPhotoGalleryCarousel').on('slid.bs.carousel', function () {
       
		that = $(this);
		var end = $('.carousel-inner .item', this).length - 1;

		var prev = $(that.find('.active').prev()).index();
		var curr = $(that.find('.active')).index();
		var next = $(that.find('.active').next()).index();

		prev = (prev<0)? end : prev;
		next = (next<0)? 0 : next;

		carouselPhotoGallery.set_prev(prev);
		carouselPhotoGallery.set_curr(curr);
		carouselPhotoGallery.set_next(next);

    });
    /*ON DOC READY RESIZE CAROUSEL IMAGE*/
	$('.carousel-img-fill', this).css('height','100%');

	/*MODAL LOADING CAROUSEL IMAGE*/
	$.each( $('#myPhotoGalleryCarousel img'), function( key, value ) {
		var last = $('#myPhotoGalleryCarousel img').length-1;

		if (last > 3 && key == 3) {
			$('#myPhotoGalleryCarousel img').eq(key).on('load', function(){
				$('#imgLoading').removeClass('show');
			});
		};

	});
	// Center modal vertically in window
	setTimeout(function() {
	    var $dialog = $('#imgLoading').find(".modal-dialog");
	    var $img = $('#imgLoading').find(".img-responsive");
	    var offset_h = ($(window).height() - $dialog.height()) / 2;
	    var offset_w = ( $dialog.width() - $img.width() ) / 2 ;
	    $($dialog).css("padding-top", offset_h);
	    $($dialog).css("left", offset_w);

	}, 10);
	//Ako i posle 3sec ne se isklucil modalot Loading... - isklucigo!
	setTimeout(function(){ $("#imgLoading").removeClass('show'); console.log('HIDE'); }, 3000);
	//Manual Close x
	$('div#imgLoading button.close').on('click', function(){
		$('#imgLoading').removeClass('show');
	});

	
	

	/* BIG PICTURE RETURN URL CLOSE BUTTON */
	var hci = parseInt($('#hidden_carousel_index').val());//globalno
	var hidden_carousel_index = (typeof(hci) === 'undefined')? 0 : hci;
	var page_load = false;
	if (!page_load) {//singleton
		$('#myPhotoGalleryCarousel').carousel(hidden_carousel_index);
		page_load = true;
	};
	$('#myPhotoGalleryCarousel').on('slid.bs.carousel', function () {//zapisi vo global obj carouselPhotoGallery
		var carouselData = $(this).data('bs.carousel');
		var currentIndex = $('.carousel-inner .active', this).index();
		var total = $('.carousel-inner .item', this).length;

		carouselPhotoGallery.set_index(currentIndex);
		carouselPhotoGallery.set_count(total);

	});
	$('#myPhotoGalleryCarousel .carousel-inner .item .carousel-caption h2 a').click(function(e){
		e.preventDefault();

		var href = $(this).attr('href');

		var site_url = $('#hidden_site_url').val();
		var photo_category = $('#hidden_photo_category').val();
		var carousel_index = carouselPhotoGallery.get_index();
		var return_url = site_url + photo_category + '/' + carousel_index;

		var form = $(
			'<form action="' + href + '" method="post">' +
				'<input type="text" name="return_url" value="' + return_url + '" />' +
			'</form>'
			);
		$('body').append(form);
		form.submit();
	});
	/*BIG PHOTO FULL SCREEN*/
	function fullscreen(){
		var docElement, request;

	    docElement = document.documentElement;
	    request = 
	    	docElement.requestFullScreen || 
	    	docElement.webkitRequestFullScreen || 
	    	docElement.mozRequestFullScreen || 
	    	docElement.msRequestFullScreen
	    ;

	    if(typeof request!="undefined" && request){
	        request.call(docElement);
	    }
	}
	function fullscreenExit(){
	    var docElement, request;

	    docElement = document;
	    request = 
	    	docElement.cancelFullScreen || 
	    	docElement.webkitCancelFullScreen || 
	    	docElement.mozCancelFullScreen || 
	    	docElement.msCancelFullScreen || 
	    	docElement.exitFullscreen
	    ;

	    if(typeof request!="undefined" && request){
	        request.call(docElement);
	    }
	}/*end fun fullscreenExit()*/
	$("#fullscreenDiv a.fullscreen").on('click', function() {

		$that = $(this);

		$that.toggleClass('fullscreen-resize-full');
		
		$('i', this).toggleClass('glyphicon-resize-full glyphicon-resize-small');

		if($that.is(".fullscreen-resize-full")){
			fullscreenExit();
		} else {
			fullscreen();
		}

	});

	$('#myPhotoGalleryCarousel').click(function(){
		/*OVA E ZA TEST*/
		// alert(carouselPhotoGallery.get_index());
		// alert(carouselPhotoGallery.get_count());


		// var fill_divs = $('#myPhotoGalleryCarousel div.carousel-inner div.fill');

		// console.log(carouselPhotoGallery.get_index());
		// console.log(fill_divs[carouselPhotoGallery.get_index()]);

		// console.log(carouselPhotoGallery.get_prev() + ', ' + carouselPhotoGallery.get_curr() + ', ' +carouselPhotoGallery.get_next() + ' | ' );
	});

	$('#myPhotoGalleryCarousel').on('slid.bs.carousel', function () {
       
/*I OVA E ZA TEST*/

// 		var fill_divs = $('div.carousel-inner div._fill', this);
// 		var attr_style_url = $(fill_divs[carouselPhotoGallery.get_index()]).css('background-image');
// 		var src = get_image_src($(fill_divs[carouselPhotoGallery.get_curr()]).css('background-image')); 

// $('#prev').attr('src', get_image_src($(fill_divs[carouselPhotoGallery.get_prev()]).css('background-image')));
// $('#curr').attr('src', get_image_src($(fill_divs[carouselPhotoGallery.get_curr()]).css('background-image')));
// $('#next').attr('src', get_image_src($(fill_divs[carouselPhotoGallery.get_next()]).css('background-image')));

		$('.carousel-img-fill', this).css('height','100%');

    });

});/*end document ready*/

function get_image_src(attr_style_url){
	return attr_style_url.substring(4, attr_style_url.length-1); 
}