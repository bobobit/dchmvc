<?php
/*
|--------------------------------------------------------------------------
| Site Name
|--------------------------------------------------------------------------
| Name of your web site.
| 
*/
$config['site_name'] = 'Prilep Panorama';

/*
|--------------------------------------------------------------------------
| Meta Data
|--------------------------------------------------------------------------
| Meta Title is the title of a web page, you can see title in browser tab. It helps both users and search engines.
| Meta Description is a short and precise description of the content of a page. It's longer than meta title. It helps search engine greatly.
| Meta Author
| Meta Keywords
| 
*/
$config['meta_title'] = 'PrilepPanorama - Panoramic view of Prilep, Republic of Macedonia';
$config['meta_description'] = 'Photo Gallery describing how I view Prilep the city in Republic of Macedonia. Panoramic view of best places in Prilep surrounded by beautiful mountains and hills.';
$config['meta_author'] = 'Влатко Илиоски';
$config['meta_keywords'] = 'Prilep Panorama Republic of Macedonia';