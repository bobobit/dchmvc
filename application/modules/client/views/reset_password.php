<!-- Page Content -->
<div class="container">
  <div class="row">
      <div class="col-lg-4 col-md-offset-4">
          <div class="login-panel panel panel-default">
              <div class="panel-heading">
                  <h3 class="panel-title"><?php echo lang('reset_password_heading');?></h3>
                  <?php if($message) { ?>
                  <i class="text-danger"><?php echo $message; ?></i>
                  <?php } ?>
              </div>
              <div class="panel-body">
                  <?php echo form_open(site_url('client/reset_password/' . $code), array('role' => 'form', 'id' => 'reset_password_form'));?>
                  	<fieldset>
                          <div class="form-group">
                              <input 
                              	class="form-control" 
                              	name="<?php echo $new_password['name']; ?>" 
                              	type="<?php echo $new_password['type']; ?>" 
                              	placeholder="New Password" 
                              	pattern="<?php echo $new_password['pattern']; ?>"
                              	title="Password must be eight characters or more"
                              	autofocus>
                          </div>
                          <div class="form-group">
                              <input 
                              	class="form-control" 
                              	name="<?php echo $new_password_confirm['name']; ?>" 
                              	type="<?php echo $new_password_confirm['type']; ?>" 
                              	placeholder="Confirm New Password"
                              	pattern="<?php echo $new_password_confirm['pattern']; ?>"
                              	title="Password must be eight characters or more">
                          </div>


							<?php echo form_input($user_id);?>
							<?php echo form_hidden($csrf); ?>


					      <div class="form-group">
                                <?php echo form_submit(array(
                                        'type'        => 'submit',
                                        'name'        => 'submit',
                                        'class'       => 'btn btn-lg btn-success btn-block',
                                        'id'          => 'submit',
                                        'value'       => lang('reset_password_submit_btn'),
                                      ));
                                ?>
                          </div>
                      </fieldset>
                  <?php echo form_close();?>

                  
                  <div class="row">
                    <div class="col-lg-8">
                      <a   href="<?php echo site_url('client/login');?>">Log In</a>
                    </div>
                    <div class="col-lg-4">
                      <a class="pull-right" href="<?php echo site_url();?>">cancel</a>
                    </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<!-- /.container -->