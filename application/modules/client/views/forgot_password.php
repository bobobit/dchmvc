<!-- Page Content -->
<div class="container">
  <div class="row">
      <div class="col-lg-6 col-md-offset-3">
          <div class="login-panel panel panel-default">
              <div class="panel-heading">
                  <h3 class="panel-title"><?php echo lang('forgot_password_heading');?></h3>
                  <p><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>
                  <?php if($message != false){ ?>
                  <div id="infoMessage" class="alert alert-warning">
                  <?php echo $message;?>
                  </div>
                  <?php } ?>
              </div>
              <div class="panel-body">
                  <?php echo form_open(site_url('client/forgot_password'), array('role' => 'form', 'id' => 'forgot_password_form'));?>
                      <fieldset>
                          <div class="form-group">
                              <label for="email"><?php echo sprintf(lang('forgot_password_email_label'), $identity_label);?></label>
                              <input class="form-control"  name="<?php echo $email['name']; ?>" type="email" autofocus>
                          </div>
                          <div class="form-group">
                                <?php echo form_submit(array(
                                        'type'        => 'submit',
                                        'name'        => 'submit',
                                        'class'       => 'btn btn-success btn-block',
                                        'id'          => 'submit',
                                        'value'       => lang('forgot_password_submit_btn'),
                                      ));
                                ?>
                          </div>
                      </fieldset>
                  <?php echo form_close();?>

                  
                  <div class="row">
                    <div class="col-lg-8">
                      <a   href="<?php echo site_url('client/login'); ?>"><?php echo lang('login_heading');?></a>
                    </div>
                    <div class="col-lg-4">
                      <a class="pull-right" href="<?php echo site_url();?>">cancel</a>
                    </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<!-- /.container -->