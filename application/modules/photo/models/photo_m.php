<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     DC HMVC
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Photo_m extends MY_Model
{

    protected $_table_name = 'photos';
    protected $_order_by = 'position';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id($id){
        $where = array('id' => $id);
        return parent::get_by($where, $single = TRUE);
    }

    public function get_by_category($category){
        $where = array('category' => $category);
        return parent::get_by($where, $single = FALSE);
    }

    public function get_by_category_id($category_id){
        $where = array('category_id' => $category_id);
        return parent::get_by($where, $single = FALSE);
    }

    public function get_by_url($url){
        $where = array('url' => $url);
        return parent::get_by($where, $single = TRUE);
    }

}