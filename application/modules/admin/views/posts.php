<!-- Page Content -->
<style type="text/css">
  .container-fluid{
    margin: 0;
    padding: 0;
  }  
  #page-wrapper{
    margin: 0;
    padding-right: 33px;
  }
</style>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-offset-2 col-lg-10">
  
              <h2>Posts </h2>

              <iframe id="iframeBackend"  name="iframeBackend"
              width="100%"
              src="<?php echo site_url('admin/iframe_posts'); ?>"
              frameborder="0"
              scrolling="yes" 
              onload='javascript:resizeIframe(this);'
              >
              </iframe>

            </div>
        </div>
    </div>
</div>
<script language="javascript" type="text/javascript">
  function resizeIframe(obj) {
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
</script>