<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     DC HMVC
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Page extends Frontend_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('page_m');
        $this->data['pages'] = $this->page_m->get_nested();

        // Fetch the page template
        $segment =  ($this->uri->segment(1))? $this->uri->segment(1) : '';

        if ($segment !='') {

            $this->data['page'] = $this->page_m->get_by_slug($segment);

            if (empty($this->data['page'])) {
                $this->_error_404();
                return;
            }
            
            $template = $this->data['page']->template;
            if ($template == 'custom') {
                $method = '_' . $segment;
                if (method_exists($this, $method)) {
                    //Call private _method()
                    $this->$method();
                }
                else {
                    //Ne e napravena _custom() funkcija za ovaa strana
                    $this->_error_404();
                }
            } elseif ($template == 'photo_gallery') {
                //template page 
                $this->_photo_gallery($segment);
            } else {
                //Page  does not exist
                $this->_error_404();
            }

        } else {
            //Base URL index view (NO uri segment)
            $this->_index();
        }

    }

    public function index() {/*ova mora da e prazno oti se povikuva po default*/}

    public function _index() {

/*        $page = $this->page_m->get_by_slug('index');
        $this->load->model("photo/photo_m");

        $data['subview_data'] = new stdClass();

        if (count($page)) {
            $data['subview_data'] = $page;
            $photos = $this->photo_m->get_by_category_id($page->id);
        } else {
            $data['subview_data']->photos = array();
            $data['subview_data']->layout = 'basic';
        }

        $this->_render_page('index', $data);*/
        $this->_home();

    }

    public function _home() {
        
        $page = $this->page_m->get_by_slug('home');
        $this->load->model("photo/photo_m");
        $photos = $this->photo_m->get_by_category_id($page->id);


        $data['subview_data'] = new stdClass();
        $data['subview_data'] = $page;
        $data['subview_data']->photos = $photos;


        $this->_render_page('home', $data);

    }

    public function _about() {

        $data['subview_data'] = $this->page_m->get_by_slug('about');

        $this->_render_page('about', $data);

    }

    private function _right_sidebar(){

        $rs_subview_data = new stdClass();

        $this->load->model('blog/post_m');
        $rs_subview_data->recent_posts = $this->post_m->get_recent(5);

        $right_sidebar = new stdClass();
        $right_sidebar->rs_module = strtolower(get_class($this));
        $right_sidebar->rs_subview = 'right_sidebar';
        $right_sidebar->rs_subview_data = $rs_subview_data;

        return $right_sidebar;
    }

    public function _contact() {

        $data['subview_data'] = $this->page_m->get_by_slug('contact');
        $data['right_sidebar'] = $this->_right_sidebar();

        $this->_render_page('contact', $data);

    }

    public function _photo_gallery($slug) {

        $page = $this->page_m->get_by_slug($slug);
        $this->load->model("photo/photo_m");
        $photos = $this->photo_m->get_by_category_id($page->id);


        $data['subview_data'] = new stdClass();
        $data['subview_data'] = $page;
        $data['subview_data']->photos = $photos;

        $second_uri_segment = intval($this->uri->segment(2));
        $carousel_index = ($second_uri_segment)? $second_uri_segment : 0;
        $data['subview_data']->carousel_index = $carousel_index;

        // TO DO: Ova ne mi sluzi oti vekje e ispisan headerot, a vonego i title i meta
        // treba nekako porano da go smenam meta title ???
        $this->data['meta']['title'] = 'Photo Gallery - Prilep Panorama';

        $this->_render_page('photo_gallery', $data);

    }

    public function _error_404() {

        $data['module'] = get_class($this);
        $data['subview'] = 'errors/error_404';
        $data['heading'] = '404 Page Not Found';
        $data['message'] = '<p>The page you requested was not found.</p>' ;
        $data['subview_data'] = array();

        echo Modules::run('layout/_basic', $data);

    }

    function _render_page($view, $data=null) {

        $data['module'] = strtolower(get_class($this));
        $data['subview'] = $view;

        $layout = $data['subview_data']->layout;

        echo Modules::run('layout/_'.$layout, $data);

    }

}