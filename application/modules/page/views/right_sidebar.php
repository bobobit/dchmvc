<div class="row">
	<div class="col-lg-12">
		<h3>Recent Posts</h3>
		<ul>
		<?php foreach ($recent_posts as $pst) { ?>
		<li>
			
		<a href="<?php echo site_url('blog/post/' . $pst->id); ?>">
			<span>
				<?php echo $pst->title; ?>
			</span>
		</a>
		</li>
		<?php }/*end foreach recent_posts*/ ?>
		</ul>
	</div>
</div>
