<style>
    .carousel-img-fill {
        /*max-width: 100%;*/
        height: 100%;
        width: auto;
        display: block;
        margin: 0 auto;
    }

    #imgLoading, 
    #imgLoading .modal-content, 
    #imgLoading .modal-body {


        /*background-color: rgba(0,0,0, 0.001);*/
        border-radius: 50%;
        
    } 

    #imgLoading .modal-content {
        color:gray;
        opacity: 0.8;
        filter: alpha(opacity=80); /* For IE8 and earlier */
        display: table;
        /*padding: 10px;*/
    }

    #myModalTransparentBackround {
        position: fixed;
        left: 0;
        top: 0;
        max-width: 100%;
        max-height: 100%;
        height: 100%;
        width: 100%;
        display: block;
        margin: 0 auto;
        background-color: rgba(15,15,15, 0.8);
        z-index: -1;
    }
</style>

<div class="modal show" id="imgLoading" tabindex="-1" role="dialog" aria-labelledby="myModalTransparentBackround" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <!-- Loading Image -->
        <img class="img-responsive center-block" src="<?php echo site_url('assets/img/ajax-loader-big.gif'); ?>" alt="Image Loading...">
        <div id="myModalTransparentBackround"></div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Carousel
================================================== -->
<!-- Full Page Image Background Carousel Header -->
<header id="myPhotoGalleryCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
    <?php foreach ($photos as $key => $photo) { ?>
        <li data-target="#myPhotoGalleryCarousel" data-slide-to="<?php echo $key; ?>" class="<?php if($key==0){ echo 'active';} ?>"></li>
    <?php } ?>   
    </ol>

    <!-- Wrapper for Slides -->
    <div class="carousel-inner">
    <?php foreach ($photos as $key => $photo) { ?>
        <div class="item <?php if($key==0){ echo 'active';} ?>">
            <!-- Set the first background image using inline CSS below. -->
            <?php $url = "'" . site_url('assets/img/' . $photo->url) . "'"; ?>
            <!-- <div class="fill" style="background-image:url(<?php echo $url; ?>);"></div> -->
            <img class="carousel-img-fill center-block" src="<?php echo site_url('assets/img/' . $photo->url); ?>" alt="">
            <div class="carousel-caption">
                <h2><?php echo anchor(site_url('photo/preview/' . $photo->id), $photo->title); ?></h2>
            </div>
        </div>
    <?php } ?>

    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myPhotoGalleryCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myPhotoGalleryCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
    
<input type="hidden" name="hidden_site_url" id="hidden_site_url" value="<?php echo site_url(); ?>">
<input type="hidden" name="hidden_photo_category" id="hidden_photo_category" value="<?php echo $slug; ?>">
<input type="hidden" name="hidden_carousel_index" id="hidden_carousel_index" value="<?php echo $carousel_index; ?>">

</header>
<!-- 
<div
style="
width: 300px,
height: 100px,
position: fixed,
left: 0,
bottom: 0
"
>
<img id="prev" src="" alt="" style="width:100px">
<img id="curr" src="" alt="" style="width:100px">
<img id="next" src="" alt="" style="width:100px">
</div>
 -->