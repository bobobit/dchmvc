<?php
/*
|--------------------------------------------------------------------------
| Meta Data
|--------------------------------------------------------------------------
| Meta Title is the title of a web page, you can see title in browser tab. It helps both users and search engines.
| Meta Description is a short and precise description of the content of a page. It's longer than meta title. It helps search engine greatly.
| Meta Author
| Meta Keywords
| 
*/
$config['meta_title'] = 'Blog Prilep Panorama';
$config['meta_description'] = 'Blog Post of Prilep Panoramic photo Gallery';
$config['meta_keywords'] = 'Prilep, Blog, Blog post, Macedonia';