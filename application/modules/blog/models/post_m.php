<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     DC HMVC
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Post_m extends MY_Model
{

    protected $_table_name = 'posts';
    protected $_order_by = 'created DESC';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id($id){
        $where = array('id' => $id);
        return parent::get_by($where, $single = TRUE);
    }

    public function get_recent($limit){
        
        $this->db->limit($limit);

        return parent::get($id = NULL, $single = FALSE);
    }

    public function get_post_like($search_query){
        
        $this->db->select('id, title, created');
        $this->db->like('title', $search_query);
        $this->db->or_like('body', $search_query);

        return parent::get($id = NULL, $single = FALSE);
    }

    public function get_title_like($search_query){
        
        $this->db->select('id, title, created');
        $this->db->like('title', $search_query); 

        return parent::get($id = NULL, $single = FALSE);
    }

    public function get_date_like($search_query){
        
        $this->db->select('id, title, created');
        $this->db->like('created', $search_query); 

        return parent::get($id = NULL, $single = FALSE);
    }


    public function get_paginated_posts ($pagination_id, $per_page) {
        $limit = $per_page;
        $offset = $pagination_id;
        $this->db->limit($limit, $offset);
        return parent::get($id = NULL, $single = FALSE);
    }


    public function get_by_category_id($category_id){

/*
SELECT * 
FROM posts 
JOIN post_category 
ON posts.id=post_category.post_id 
WHERE post_category.category_id=1

eh1($this->db->last_query());
*/


        $this->db->select('*');
        $this->db->from('posts');
        $this->db->join('post_category', 'posts.id=post_category.post_id');
        $this->db->where('post_category.category_id', $category_id); 

        return $this->db->get()->result();
    }

}