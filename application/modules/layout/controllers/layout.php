<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     DC HMVC
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Layout extends MY_Controller
{

    public function __construct() {
        parent::__construct();
        // $this->data = array();//ovaa niza treba da e deklarirana vo MY_Controller
        $this->load->model("page/page_m");
        $this->data['menu'] = $this->page_m->get_public_menu();

    }

    public function index() {
        redirect('/');
    }

    public function _basic($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('basic', $data);
    }

    public function _left_sidebar($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('left_sidebar', $data);
    }

    public function _right_sidebar($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('right_sidebar', $data);
    }

    public function _both_sides_sidebar($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('both_sides_sidebar', $data);
    }

    public function _admin($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('admin', $data);
    }
    
    public function _authentication($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('authentication', $data);
    }  

    public function _photo_gallery($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('photo_gallery', $data);
    }

    public function _blog($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('blog', $data);
    }

    public function _bigpicture($data) {
        $data['module'] = strtolower($data['module']);
        $this->load->view('bigpicture', $data);
    }


}