<?php $this->load->view('partials/header', $this->data); ?>
<?php $this->load->view('navigation', $this->data); ?>
<style>
#otstapuvanje{
	min-height: 50px;
	margin-bottom: 20px;
	border: 1px solid;
}
</style>
<div id="otstapuvanje">
</div>

<!-- Page Content -->
<div class="container"><?php /* ovde treba da pocni sodrzinata na stranata, a se zatvora vo footer*/ ?>
<div class="row">
	<div <div class="col-lg-9">
		<?php $this->load->view($module . '/' . $subview, $subview_data); ?>
	</div>

	<div class="col-lg-3">
		<?php $this->load->view('partials/right', $right_sidebar); ?>
	</div>
</div>

<?php $this->load->view('partials/footer'); ?>