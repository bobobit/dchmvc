<?php $this->load->view('partials/header', $this->data); ?>
<?php $this->load->view('navigation', $this->data); ?>
<style>
#otstapuvanje{
	min-height: 50px;
	margin-bottom: 20px;
	border: 1px solid;
}
</style>
<div id="otstapuvanje">
</div>

<!-- Page Content -->
<div class="container row"><?php /* ovde treba da pocni sodrzinata na stranata, a se zatvora vo footer*/ ?>
	<div class="col-lg-2">
		<?php $this->load->view('partials/left'); ?>
	</div>

	<div <div class="col-lg-10">
		<?php $this->load->view($module . '/' . $subview, $subview_data); ?>
	</div>

<?php $this->load->view('partials/footer'); ?>