<!DOCTYPE html>
<html lang="en" 
style="
background: url(<?php echo site_url('assets/img/' . $photo->url); ?>) no-repeat center center fixed;
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;
" 
class="full" 
>
<!-- Make sure the <html> tag is set to the .full CSS class. Change the background image in the full.css file. -->

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <?php foreach ($meta as $meta_name => $meta_content) { ?>
        <?php if ($meta_name == 'title') { ?>
            <title><?php echo $meta_content; ?></title>
        <?php } else { ?>
            <meta name="<?php echo $meta_name; ?>" content="<?php echo $meta_content; ?>">
        <?php } ?>
    <?php } ?>

	<!-- Bootstrap Core CSS -->
    <link href="<?php echo site_url('assets/themes/bigpicture/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo site_url('assets/themes/bigpicture/css/the-big-picture.css'); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>