<?php if (!empty($photos)) { ?>
<!-- Carousel
================================================== -->
<!-- Full Page Image Background Carousel Header -->
<header id="myCarousel" class="carousel slide carousel-fade">
    <!-- Indicators -->
    <ol class="carousel-indicators">
    <?php foreach ($photos as $key => $photo) { ?>
        <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" class="<?php if($key==0){ echo 'active';} ?>"></li>
    <?php } ?>   
    </ol>

    <!-- Wrapper for Slides -->
    <div class="carousel-inner">
    <?php foreach ($photos as $key => $photo) { ?>
        <div class="item <?php if($key==0){ echo 'active';} ?>">
            <!-- Set the first background image using inline CSS below. -->
            <?php $url = "'" . site_url('assets/img/' . $photo->url) . "'"; ?>
            <div class="fill" style="background-image:url(<?php echo $url; ?>);"></div>
            <div class="carousel-caption">
                <h2><?php echo anchor(site_url(strtolower($photo->title)), $photo->title); ?></h2>
            </div>
        </div>
    <?php } ?>
    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>

</header>

<!-- /.carousel -->
<?php /*END IF PHOTOS*/ } else { ?>
<div style="min-height: 50px;"></div>
<?php } /*ELSE IF NOT PHOTOS*/ ?>