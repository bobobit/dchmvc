  <!-- Navigation -->
 <nav class="navbar navbar-inverse navbar-fixed-bottom hidden-lg hidden-md hidden-sm " role="navigation" id="bigpictureNav">
   <div class="container">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="navbar-header">
       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bigpicture-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
       </button>
       <a class="navbar-brand" href="<?php echo site_url('/'); ?>">Prilep Panorama</a>
      </div><!-- /.navbar-header -->
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bigpicture-navbar-collapse">
       
<?php echo get_menu($menu); ?>

     </div><!-- /.navbar-collapse -->


   </div><!-- /.container -->
 </nav>