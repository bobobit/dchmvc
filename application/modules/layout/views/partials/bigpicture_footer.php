
    <!-- jQuery -->
	<script src="<?php echo site_url('assets/themes/bigpicture/js/jquery.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo site_url('assets/themes/bigpicture/js/bootstrap.min.js'); ?>"></script>

    <!-- Custom Scripts -->
	<script src="<?php echo site_url('assets/js/scripts.js'); ?>"></script>

</body>

</html>