  <!-- Navigation -->
 <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
   <div class="container">
     <!-- Brand and toggle get grouped for better mobile display -->
     <div class="navbar-header">
       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
       </button>
       <a class="navbar-brand" href="<?php echo site_url('/'); ?>">Prilep Panorama</a>
      </div><!-- /.navbar-header -->
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
       
<?php echo get_menu($menu); ?>

    <?php if ($this->session->userdata('user_id')) { ?>
    <ul class="nav navbar-nav pull-right">
      <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="glyphicon glyphicon-user"></i>
          Member <?php echo $this->session->userdata('username'); ?> <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo site_url('client/logout'); ?>">
            <i class="glyphicon glyphicon-off"></i> Log Out</a>
          </li>
        </ul>
      </li>
    </ul>
    <?php } else { ?>
    <!-- Nav LogIn  -->
    <div class="navbar-form navbar-right">
      <form action="<?php echo site_url('client/login'); ?>" role="search" method="post">
          <div class="form-group">
              <input class="form-control visible-lg" name="identity" placeholder="E-Mail" type="text">
          </div>
          <div class="form-group">
              <input class="form-control visible-lg" name="password" placeholder="Password" type="password">
          </div>
          <button type="submit" class="btn btn-success">Log In</button>
          <a class="btn btn-success" href="<?php echo site_url('client/signup'); ?>">Sign Up</a>
      </form>
    </div>
    <?php }/*end if user*/ ?>


     </div><!-- /.navbar-collapse -->


   </div><!-- /.container -->
 </nav>