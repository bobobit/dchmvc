<?php $this->load->view('partials/header', $this->data); ?>
<?php $this->load->view('navigation', $this->data); ?>

<?php $this->load->view($module . '/' . $subview, $subview_data); ?>

<?php $this->load->view('partials/gallery_footer'); ?>
