<?php $this->load->view('partials/blog_header', $this->data); ?>
<?php $this->load->view('navigation', $this->data); ?>
<!-- Page Content -->
<div class="container"><?php /* ovde treba da pocni sodrzinata na stranata, a se zatvora vo footer*/ ?>
<div class="row">
	<div <div class="col-lg-9">
		<?php $this->load->view($module . '/' . $subview, $subview_data); ?>
	</div>

	<div class="col-lg-3">
		<?php $this->load->view('partials/right', $right_sidebar); ?>
	</div>
</div>

<?php $this->load->view('partials/blog_footer'); ?>