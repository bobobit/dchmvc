<?php $this->load->view('partials/bigpicture_header', $this->data); ?>

<?php $this->load->view('partials/bigpicture_navigation', $this->data); ?>
<div id="windowButtons">
    <div class="fullscreenDiv" id="fullscreenDiv">
    <a class="fullscreen fullscreen-resize-full" href="#"><i class="glyphicon glyphicon-resize-full"></i></a>

    <!-- 
        <a href="#" class="fullscreen">fullscreen</a>
        <a href="#" class="fullscreenExit">Exit fullscreen</a> -->
    </div><!-- ./div button closePreview -->
    <div class="closePreview" id="closePreview">
        <a class="close" href="<?php echo $return_url; ?>">&times;</a>
    </div><!-- ./div button closePreview -->
</div>

<div class="btn btn-default btn-xs hidden-xs navToggle" id="navToggle">
    <span class="glyphicon glyphicon-chevron-down"></span>
</div><!-- ./div button switchNav -->
<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-md-6 col-sm-12">

		<?php $this->load->view($module . '/' . $subview, $subview_data); ?>

        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->

<?php $this->load->view('partials/bigpicture_footer'); ?>