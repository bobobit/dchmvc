<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     DC HMVC
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Renamethis_m extends MY_Model
{

    protected $_table = 'renamethis';

    public function __construct()
    {
        parent::__construct();
    }

}