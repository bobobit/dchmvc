<?php defined('BASEPATH') OR exit('No direct script access allowed');

/** 
 * @package     DC HMVC
 * @author      Bobo
 * @copyright   (c) 2015, Development
 * @since       Version 0.1
 */

class Datepicker extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = array();
        $this->load->view('index', $data);
    }

    public function default_funcionality(){

        $this->load->view('datepicker');

    }

}