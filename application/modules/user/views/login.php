<!-- Page Content -->
<div class="container">
  <div class="row">
      <div class="col-lg-4 col-md-offset-4">
          <div class="login-panel panel panel-default">
              <div class="panel-heading">
                  <h3 class="panel-title">Please Sign In</h3>
                  <?php if($this->session->flashdata('message')) { ?>
                  <i class="text-danger"><?php echo $this->session->flashdata('message'); ?></i>
                  <?php } ?>
              </div>
              <div class="panel-body">
                  <?php echo form_open("user/login", array('role' => 'form', 'id' => 'login_form'));?>
                      <fieldset>
                          <div class="form-group">
                              <input class="form-control" placeholder="E-mail" name="<?php echo $identity['name']; ?>" type="<?php echo $identity['type']; ?>" autofocus>
                          </div>
                          <div class="form-group">
                              <input class="form-control" placeholder="Password" name="<?php echo $password['name']; ?>" type="<?php echo $password['type']; ?>" value="">
                          </div>
                          <div class="checkbox">
                              <label>
                                  <input name="remember" id="remember" type="checkbox" value="Remember Me">Remember Me
                              </label>

                          </div>
                          <div class="form-group">
                                <?php echo form_submit(array(
                                        'type'        => 'submit',
                                        'name'        => 'submit',
                                        'class'       => 'btn btn-lg btn-success btn-block',
                                        'id'          => 'submit',
                                        'value'       => lang('login_submit_btn'),
                                      ));
                                ?>
                          </div>
                      </fieldset>
                  <?php echo form_close();?>

                  <a   href="forgot_password?forgot_password=1"><?php echo lang('login_forgot_password');?></a>
              </div>
          </div>
      </div>
  </div>
</div>
<!-- /.container -->
