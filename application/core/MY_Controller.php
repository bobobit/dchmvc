<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

	public $data = array();
	
	public function __construct(){
	
		parent::__construct();
		$this->data['errors'] = array();
		$this->data['site_name'] = config_item('site_name');
		$this->data['meta'] = array(
			'title' => config_item('meta_title'),
			'description' => config_item('meta_description'),
			'author' => config_item('meta_author'),
			'keywords' => config_item('meta_keywords')
		);

		/*Ova se izvrsuva samo ednas ko ce se startuva browserot odnosno sesijata
		(vo CI config mi e podeseno sesijata da se unisti zo gasenje na browserot)*/
		$session_singleton = $this->session->userdata('session_singleton');

		if (! $session_singleton) {
			$this->session->set_userdata(array(
		       'session_singleton' => TRUE
		   	));
		}

	}/*end construct*/
/*END CLASS MY_Controller*/}