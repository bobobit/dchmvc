-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2015 at 12:53 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `prileppanorama_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `url`, `priority`) VALUES
(1, 0, 'Sunset', 'sunset', 7),
(2, 0, 'Mountain', 'mountain', 6),
(3, 0, 'Field', 'field', 5),
(4, 0, 'Urban', 'urban', 4),
(5, 0, 'Prilep', 'prilep', 1),
(6, 0, 'Macedonia', 'macedonia', 2),
(7, 0, 'Skopje', 'skopje', 3),
(8, 5, 'Prilep Plostad', 'prilepplostad', 8),
(9, 7, 'Skopje Plostad', 'skopjeplostad', 9),
(10, 5, 'Prilepska Reka', 'prilepskareka', 10),
(11, 5, 'Prilepski Tutun', 'prilepskitutun', 11),
(12, 0, 'Sport', 'sport', 12);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('2fe11f67ee91536d688aa32bb85ba17b', '::1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.91 Safari/537.36', 1422140885, 'a:7:{s:9:"user_data";s:0:"";s:17:"session_singleton";b:1;s:8:"identity";s:22:"bozidar.ilio@gmail.com";s:8:"username";s:13:"administrator";s:5:"email";s:22:"bozidar.ilio@gmail.com";s:7:"user_id";s:1:"3";s:14:"old_last_login";s:10:"1422114197";}');

-- --------------------------------------------------------

--
-- Table structure for table `example_3`
--

CREATE TABLE IF NOT EXISTS `example_3` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `category` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `position` int(4) unsigned NOT NULL,
  `price` float NOT NULL,
  `qty` int(11) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `thumb` varchar(100) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `example_3`
--

INSERT INTO `example_3` (`id`, `name`, `title`, `category`, `description`, `position`, `price`, `qty`, `url`, `thumb`, `category_id`, `priority`) VALUES
(1, 'babsirinc', '', '', '', 0, 0, 0, 'ac926-babsirinc.jpg', 'thumb__ac926-babsirinc.jpg', 9, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `permission_level` int(11) NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permission_level`, `description`) VALUES
(1, 'admin', 4, 'Administrator'),
(2, 'visitor', 1, 'Visitor'),
(3, 'member', 2, 'Member'),
(4, 'creator', 3, 'Creator'),
(5, 'forbidden', 0, 'Forbidden');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL,
  `slug` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `template` varchar(100) NOT NULL,
  `layout` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `contents` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `parent_id`, `slug`, `name`, `title`, `template`, `layout`, `position`, `visible`, `contents`) VALUES
(1, 0, 'home', 'Home', 'Home Page', 'custom', 'basic', 1, 1, 'This is contents of a Home Page.\r\n"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt"\r\n'),
(2, 0, 'about', 'About', 'About Us Page', 'custom', 'left_sidebar', 2, 1, '<p>\r\n	About Us Page contents</p>\r\n<p>\r\n	<b>It&#39;s one of the most</b> important elements on a company&#39;s website and also one of the most undervalued: the ubiquitous &quot;About Us&quot; page&mdash;that section on your site that has been collecting virtual dust because you haven&#39;t bothered to read it since, well, you first wrote it.<br />\r\n	<br />\r\n	You may not be paying it much attention, but visitors to your site are. And considering that your About Us page is where the world clicks to learn about your company and the services you offer, which can mean the potential loss or gain of a customer, it deserves a little more consideration and a lot more respect.</p>\r\n'),
(3, 0, 'contact', 'Contact', 'Contact Page', 'custom', 'right_sidebar', 5, 1, '<p>\r\n	Contact Page Contents</p>\r\n<p class="single-first-p">\r\n	The purpose of any website is to get people from your target audience interested in what you&rsquo;re offering. Whether it be a product or service, 9 times out of 10, someone is going to want to communicate with you further. Because of this, in almost any industry, you&rsquo;re going to want to create a contact page.</p>\r\n<p>\r\n	For some, this is that last page on the site map where you just throw a bunch of information. You can leave it up to the person to decide how they want to contact you and what they want to contact you about. For others, this is the last attempt to get your potential customer to give you their business.</p>\r\n<p>\r\n	The contact page is much more important than many give it credit. Many basic websites just throw some numbers and e-mails up and move along. But in most cases, this is the page your customer sees before they decide they want you on their project. Or before they decide they want to visit you to purchase your product.</p>\r\n'),
(4, 0, 'gallery', 'Gallery', 'Photo Gallery', 'custom', 'basic', 3, 1, ''),
(5, 0, 'blog', 'Blog', 'Blog Page', 'custom', 'basic', 4, 1, ''),
(6, 4, 'urban', 'Urban', 'Urpan Page Photos', 'photo_gallery', 'photo_gallery', 1, 1, ''),
(7, 4, 'fields', 'Fields', 'Fields Photo Gallery', 'photo_gallery', 'photo_gallery', 2, 1, ''),
(8, 4, 'animals', 'Animals', 'Animals Page Photo Gallery', 'photo_gallery', 'photo_gallery', 3, 1, ''),
(9, 4, 'toptash', 'Toptash', 'Toptash Page', 'photo_gallery', 'photo_gallery', 6, 1, ''),
(10, 4, 'shatorovkamen', 'Satorov Kamen', 'Satorov Kamen Page', 'photo_gallery', 'photo_gallery', 7, 1, ''),
(11, 4, 'markovikuli', 'Markukule', 'Markukule Page', 'photo_gallery', 'photo_gallery', 7, 1, ''),
(12, 4, 'zlatovrv', 'Zlatovrv', 'Zlatovrv', 'photo_gallery', 'photo_gallery', 8, 1, ''),
(13, 4, 'sunset', 'Sunset', 'Sunset Photos', 'photo_gallery', 'photo_gallery', 9, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `position` int(4) unsigned NOT NULL,
  `price` float NOT NULL,
  `qty` int(11) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `thumb` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `name`, `title`, `category_id`, `description`, `position`, `price`, `qty`, `url`, `thumb`) VALUES
(1, 'starivozW', 'Urban', 1, '', 1, 0, 0, '8a907-starivozW.jpg', 'thumb__8a907-starivozW.jpg'),
(2, 'zelenagstrica', 'Animals', 1, '', 2, 0, 0, 'f2e0f-zelenagstrica.jpg', 'thumb__f2e0f-zelenagstrica.jpg'),
(4, 'lozjastrfkmnsky', 'Fields', 1, '', 3, 0, 0, '39518-lozjastrfkmnsky.jpg', 'thumb__39518-lozjastrfkmnsky.jpg'),
(5, 'dramtreskaecMkukul', 'Zlatovrv', 1, '', 4, 0, 0, 'bd2a7-dramtreskaecMkukul.jpg', 'thumb__bd2a7-dramtreskaecMkukul.jpg'),
(6, 'lordMrkkleslikovnic', 'MarkoviKuli', 1, '', 5, 0, 0, '3100a-lordMrkkleslikovnic.jpg', 'thumb__3100a-lordMrkkleslikovnic.jpg'),
(7, 'PPshtrfkmnBw', 'Shatorovkamen', 1, '', 6, 0, 0, '2cd29-PPshtrfkmnBw.jpg', 'thumb__2cd29-PPshtrfkmnBw.jpg'),
(8, 'strfsunsetoWide', 'Sunset', 1, '', 7, 0, 0, '72cfd-strfsunsetoWide.jpg', 'thumb__72cfd-strfsunsetoWide.jpg'),
(9, 'dreamon', 'Toptash', 1, '', 8, 0, 0, '37ef3-dreamon.jpg', 'thumb__37ef3-dreamon.jpg'),
(11, 'babsirinc', 'Baba', 6, '', 2, 0, 0, 'e37c1-babsirinc.jpg', 'thumb__e37c1-babsirinc.jpg'),
(12, 'branaribro', 'Brana', 6, '', 3, 0, 0, '2676c-branaribro.jpg', 'thumb__2676c-branaribro.jpg'),
(13, 'odrekcno', 'Prilepska reka', 6, '', 4, 0, 0, '3e0aa-odrekcno.jpg', 'thumb__3e0aa-odrekcno.jpg'),
(14, 'starivozW', 'Voz', 6, '', 1, 0, 0, '4eb32-starivozW.jpg', 'thumb__4eb32-starivozW.jpg'),
(15, 'belazimazutakuca', 'Se e Belo', 7, '', 0, 0, 0, 'd75be-belazimazutakuca.jpg', 'thumb__d75be-belazimazutakuca.jpg'),
(16, 'lozjastrfkmnsky', 'Lozje', 7, '', 0, 0, 0, '46b08-lozjastrfkmnsky.jpg', 'thumb__46b08-lozjastrfkmnsky.jpg'),
(17, 'orasi', 'Orev', 7, '', 0, 0, 0, '6a562-orasi.jpg', 'thumb__6a562-orasi.jpg'),
(18, 'osnezenitutnki', 'Tutun sneg', 7, '', 0, 0, 0, '9fda1-osnezenitutnki.jpg', 'thumb__9fda1-osnezenitutnki.jpg'),
(19, 'zitkiztinadpt', 'Zito', 7, '', 0, 0, 0, '9a984-zitkiztinadpt.jpg', 'thumb__9a984-zitkiztinadpt.jpg'),
(20, 'koncina', 'Konjcinja', 8, '', 0, 0, 0, 'b9c15-koncina.jpg', 'thumb__b9c15-koncina.jpg'),
(21, 'insekc', 'Kolibri', 8, '', 0, 0, 0, '5e7e5-insekc.jpg', 'thumb__5e7e5-insekc.jpg'),
(22, 'ovcibravostrfkmn', 'Ovci', 8, '', 0, 0, 0, '6c8d4-ovcibravostrfkmn.jpg', 'thumb__6c8d4-ovcibravostrfkmn.jpg'),
(23, 'spidermealinvrs', 'Pajak', 8, '', 0, 0, 0, '9a352-spidermealinvrs.jpg', 'thumb__9a352-spidermealinvrs.jpg'),
(24, 'patceovcina', 'Pateka od ovci', 8, '', 0, 0, 0, 'e62c5-patceovcina.jpg', 'thumb__e62c5-patceovcina.jpg'),
(25, 'zelenagstrica', 'Guster zelen', 8, '', 0, 0, 0, 'a12c9-zelenagstrica.jpg', 'thumb__a12c9-zelenagstrica.jpg'),
(26, 'dreamon', 'Frame', 9, '', 0, 0, 0, 'beaed-dreamon.jpg', 'thumb__beaed-dreamon.jpg'),
(27, 'toptdcule', 'Presek', 9, '', 0, 0, 0, '923f5-toptdcule.jpg', 'thumb__923f5-toptdcule.jpg'),
(28, 'dreamzracina', 'Pelagonija', 9, '', 0, 0, 0, '93388-dreamzracina.jpg', 'thumb__93388-dreamzracina.jpg'),
(29, 'DSC_0173', 'Zalez', 9, '', 0, 0, 0, '649c6-DSC_0173.JPG', 'thumb__649c6-DSC_0173.JPG'),
(30, 'idilicno', 'Diagonala', 10, '', 0, 0, 0, '8edde-idilicno.jpg', 'thumb__8edde-idilicno.jpg'),
(31, 'karpsatorofkmen', 'Karpa Satorovkamen', 10, '', 0, 0, 0, '3fab0-karpsatorofkmen.jpg', 'thumb__3fab0-karpsatorofkmen.jpg'),
(32, 'krstecesma', 'Cesma Krste Parapanski', 10, '', 0, 0, 0, 'bcef2-krstecesma.jpg', 'thumb__bcef2-krstecesma.jpg'),
(33, 'zitkiztinadpt', 'Zito', 10, '', 0, 0, 0, '0183a-zitkiztinadpt.jpg', 'thumb__0183a-zitkiztinadpt.jpg'),
(35, 'fantastikoMkuktvrdin', 'Kula', 11, '', 0, 0, 0, '57b0c-fantastikoMkuktvrdin.jpg', 'thumb__57b0c-fantastikoMkuktvrdin.jpg'),
(36, 'grstPP', 'Prilep na dlanka', 11, '', 0, 0, 0, '0e772-grstPP.jpg', 'thumb__0e772-grstPP.jpg'),
(37, 'lordMrkkleslikovnic', 'Zid', 11, '', 0, 0, 0, 'ea341-lordMrkkleslikovnic.jpg', 'thumb__ea341-lordMrkkleslikovnic.jpg'),
(38, 'Mrkkleslikovnic', 'Zalez', 11, '', 0, 0, 0, '94e87-Mrkkleslikovnic.jpg', 'thumb__94e87-Mrkkleslikovnic.jpg'),
(39, 'pattoMkukleclouds', 'Krsto', 11, '', 0, 0, 0, '9a538-pattoMkukleclouds.jpg', 'thumb__9a538-pattoMkukleclouds.jpg'),
(40, 'porta', 'Porta', 11, '', 0, 0, 0, 'dfe7d-porta.jpg', 'thumb__dfe7d-porta.jpg'),
(41, 'robotikarpiMarkklski', 'Talking Heads', 11, '', 0, 0, 0, '0f442-robotikarpiMarkklski.jpg', 'thumb__0f442-robotikarpiMarkklski.jpg'),
(42, 'sloncevoboja', 'Slonce', 11, '', 0, 0, 0, '6dfda-sloncevoboja.jpg', 'thumb__6dfda-sloncevoboja.jpg'),
(43, 'tvrdinskaperspMark', 'Kula Strazarnica', 11, '', 0, 0, 0, '1752c-tvrdinskaperspMark.jpg', 'thumb__1752c-tvrdinskaperspMark.jpg'),
(44, 'zlatovrvodMarkkleto', 'Zlatovrv', 11, '', 0, 0, 0, '6282d-zlatovrvodMarkkleto.jpg', 'thumb__6282d-zlatovrvodMarkkleto.jpg'),
(50, 'aerotreskaec', 'Treskaec', 12, '', 0, 0, 0, 'dbd4b-aerotreskaec.jpg', 'thumb__dbd4b-aerotreskaec.jpg'),
(51, 'dramtreskaecMkukul', 'Dream', 12, '', 0, 0, 0, '00fb2-dramtreskaecMkukul.jpg', 'thumb__00fb2-dramtreskaecMkukul.jpg'),
(52, 'framzltovrvshtr', 'Frame', 12, '', 0, 0, 0, '92d26-framzltovrvshtr.jpg', 'thumb__92d26-framzltovrvshtr.jpg'),
(53, 'zlatovrvoblci', 'Vo oblaci', 12, '', 0, 0, 0, 'dfb8c-zlatovrvoblci.jpg', 'thumb__dfb8c-zlatovrvoblci.jpg'),
(54, 'zelcinnana', 'Petti element', 12, '', 0, 0, 0, 'b2f71-zelcinnana.jpg', 'thumb__b2f71-zelcinnana.jpg'),
(55, 'fluidclouds1', 'Fluid Clouds', 13, '', 0, 0, 0, '9743c-fluidclouds1.jpg', 'thumb__9743c-fluidclouds1.jpg'),
(56, 'ludioblacici', 'Ludi oblaci', 13, '', 0, 0, 0, 'cfbf3-ludioblacici.jpg', 'thumb__cfbf3-ludioblacici.jpg'),
(57, 'nkajmkklsunsetce', 'Nakaj markukule', 13, '', 0, 0, 0, '80bbb-nkajmkklsunsetce.jpg', 'thumb__80bbb-nkajmkklsunsetce.jpg'),
(58, 'oblaciozlat1', 'Ozlateni oblaci', 13, '', 0, 0, 0, 'f3168-oblaciozlat1.jpg', 'thumb__f3168-oblaciozlat1.jpg'),
(59, 'pletvarskioblaci', 'Pletvarski oblaci', 13, '', 0, 0, 0, 'a3d47-pletvarskioblaci.jpg', 'thumb__a3d47-pletvarskioblaci.jpg'),
(60, 'pozatMarkklesu', 'Pozadi markukule', 13, '', 0, 0, 0, '1e255-pozatMarkklesu.jpg', 'thumb__1e255-pozatMarkklesu.jpg'),
(61, 'rcozracsirinkpt', 'Krst zraci', 13, '', 0, 0, 0, '90841-rcozracsirinkpt.jpg', 'thumb__90841-rcozracsirinkpt.jpg'),
(62, 'redrockMrkklezlzx', 'Markukule zalez', 13, '', 0, 0, 0, '41836-redrockMrkklezlzx.jpg', 'thumb__41836-redrockMrkklezlzx.jpg'),
(63, 'strfsunsetoWide', 'Sunset', 13, '', 0, 0, 0, '025f8-strfsunsetoWide.jpg', 'thumb__025f8-strfsunsetoWide.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `photo_categories`
--

CREATE TABLE IF NOT EXISTS `photo_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `photo_categories`
--

INSERT INTO `photo_categories` (`id`, `name`) VALUES
(1, 'urban'),
(2, 'animals');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  `body` longtext,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `created`) VALUES
(7, 'Вардар последен четвртфиналист во Купот', 'Вардар е последниот тим кој избори пласман во четвртфиналето во фудбалскиот Куп на Македонија.\r\n\r\n„Црвено-црните“, во реванш натпреварот од осминафиналето ја совладаа Шкендија со 3-0, со што ја надоместија негативата од 1-2 која ја имаа по првиот натпревар. Ова е втора победа на Вардар над Шкендија во изминатите три дена, во неделата вардарци победија во првенствениот дуел со 2-1.\r\n\r\nВардар на импресивен начин стигна до победата над Шкендија, а победата ја најави Асани со брилијантен гол уште во третата минута. Целата приказна околу пласманот во четвртфиналето Вардар ја заврши во периодот од 34. до 37. минута кога меѓу стрелците се запишаа Дашијан и Таневски.\r\n\r\nВо вториот дел Вардар не успеа да реализира неколку добри можности за постигнување  гол, а Ивановски во финишот ја погоди и гредата.\r\n\r\nПокрај Вардар, пласман меѓу најдобрите осум во Купот изборија Работнички, Турново, Кожув, Еуромилк, Дрита, Ренова и Тетекс.', '2014-10-22 18:10:07'),
(8, 'Фергусон: Дејвид Мојс не ја сфати големината на Јунајтед', '<p>\r\n	Еден од најдобрите тренери во фудбалската историја истакна дека поранешниот менаџер на Евертон не сфатил колкава е големината на најпопуларниот британски клуб. Пред Дејвид Мојс да биде именуван за тренер на Манчестер Јунајтед, Фергусон во својот дом одржал состанок со него, со цел да му појасни што се очекува од него на &bdquo;Олд Трафорд&ldquo;. Инаку, Фергусон одбиваше јавно да говори за &bdquo;случајот Мојс&ldquo;, односно за минатата сезона по која Манчестер Јунајтед по две децении остана без пласман во европските купови. Фергусон во дополнетото издание на својата автобиографија открива дека имал состанок во &bdquo;четири очи&ldquo; со Мојс. &bdquo;Не ја разбра големината на Манчестер Јунајтед. На професионален план за него тоа беше огромен скок. Ние секогаш игравме брзо, динамично, од проста причина што низ годините фудбалерите беа навикнати на тој начин да функционираат на теренот. Брзата игра беше наш начин да стигнеме до посакуваниот резултат.&ldquo; Дејвид Мојс на клупата на Манчестер Јунајтед се задржа осум месеци, а поради лошите резултати тој доби отказ. &bdquo;Кога резултатите се влошија, секој нов пораз за него преставуваше нов удар. Тоа го забележав по неговото однесување. Во јануари го пазаривме Хуан Мата и тоа беше потстрек за сите. Но, ние не успеавме да продолжиме во тој ритам, а Дејвид имаше се помалку време&ldquo;, пишува Фергусон. Алекс Фергусон кој 25 години беше тренер на Манчестер Јунајтед во автобиографијата ги отфрла тврдењата дека тој е виновен за именувањето и резултатите на Дејвид Мојс.</p>\r\n', '2014-12-17 06:10:51'),
(9, 'Tecnicar Lavinia - италијански електричен суперавтомобил со 800 КС', 'Некогаш, зборот „електрично возило” беше асоцијација за летаргија и бавност. Официјално, тие денови завршија.\r\n\r\nОд електричниот Harley Davidson до неверојатниот Tesla Model S P85D, сега тука е и италијанскиот суперавтомобил со целосно електричен погон и можност од 800 коњски сили.\r\n\r\nЗапознајте ја Tecnicar Lavinia SC која треба да дебитира на Top Marques Monaco Show наредната година.\r\n\r\n\r\n\r\nИако Tecnicar уште нема објавено детални технички спецификации за возилото, тоа што е познато е дека последните неколку години ги поминаа во развој на прототип на електричното возило, градски електрични возила, како и електричен воз во Сицилија. Барем имаат искуство со електрични возила.', '2014-10-22 18:22:18'),
(10, 'Битка на американските мускулести автомобили: Mustang GT против Camaro SS', 'Кога се во прашање американските „масл“ автомобили веројатно нема попретставителни модели од Ford Mustang и Chevy Camaro. Motor Trend ги зема најсилните модели на двата нови модела и реши да ги тестира во директна битка.\r\n\r\n\r\n\r\nШестата генерација на Ford Mustang GT доаѓа со 435 коњски сили од моќниот 5.0 литарски V8 агрегат и има дополнителен Performance пакет, додека петтата генерација на Chevrolet Camaro SS има 6,2 литарски V8 мотор кој испорачува 426 коњски сили и спортски пакет 1LE Performance.\r\n\r\nЕпилогот од битката може да го погледнете во видеото:', '2014-10-22 18:22:39'),
(11, 'Трка на хипер моделите - кој ќе победи?', 'Трките меѓу автомобили и мотоцикли иако се забавни, најчесто немаат неизвесност бидејќи резултатот секогаш е изразено наклонет кон еден од натпреварувачите. Сепак, оваа трка е поинаква.\r\n\r\n\r\n\r\nБританскиот магазин Autocar ги спротивстави два од трите хиперавтомобили - McLaren P1 и Porsche 918 со исклучително скапиот Ducati 1199 Panigale Superleggera.\r\n\r\nМотоциклот влегува на местото на Ferrari кој одби да го вклучи моделот Laferrari во трката. Интересно е што Ducati за својот претставник кој е изработен во лимитирана серија не сака да ја открие ни точната сила со која што располага, па вели дека има повеќе од 200 коњски сили без да прецизира.\r\n\r\nРезултатот од трката ќе мора да го видите во видеото, но ќе ви кажеме дека е прилично изедначена и интересна со неколку пресврти. Дефинитивно вреди да се погледне.', '2014-10-22 18:22:58'),
(12, 'УЕФА подготвува драстични казни за Србија и Албанија?', '<p>\r\n	Европската фудбалска унија (УЕФА) подготвува драстични казни за Србија и Албанија поради инцидентите кои се случија во Белград на мечот меѓу двете репрезентации, поради што натпреварот беше прекинат. Италијанските и француските медиуми пренесуваат дека постои голема веројатност УЕФА двете репрезентации да ги исфрли од квалификациите за пласман на ЕУРО 2016. Според истите извори, УЕФА на овој начин би испратила силна порака до сите дека она што се случи во Белград нема да биде толерирано на ниту еден стадион во Европа. Дисциплинското тело на УЕФА овој предмет ќе го разгледува на 23-ти октомври. Минатата недела Дисциплинското тело на УЕФА соопшти дека против Фудбалскиот сојуз на Албанија ќе биде поведена постапка поради прикажување на недозволен транспарент и поради тоа што албанските фудбалери одбија да се вратат на теренот и да го продолжат натпреварот. Против Фудбалскиот сојуз на Србија се води постапка по пет точки: поради лошата организација, употребата на пиротехнички средства, насилството на трибините, влегувањето на навивачите на теренот и употребата на ласери.</p>\r\n', '2014-12-31 06:23:40'),
(13, 'Set up SSH for Git', ' Set up SSH for Git\r\nSkip to end of metadata\r\n\r\n    Created by manthony, last modified by Dan Stevens [Atlassian] on May 20, 2014\r\n\r\nGo to start of metadata\r\n\r\nUp until this point, you have been using the secure hypertext transfer protocol (HTTPS) to communicate between your local system and Bitbucket. When you use HTTPS, you need to authenticate (supply a username and password) each time you take an action that communicates with the Bitbucket server. You can specify the username in the DVCS configuration file; you don''t want to store your password there though where anyone can see it. So, this means you must manually type a password when you use HTTPS with  your local repository. Who wants to do that? This page shows you how to use secure shell (SSH) to communicate with the Bitbucket server and avoid having to manually type a password.\r\n\r\nFinally, setting up an SSH identity can be prone to error. Allow yourself some time, perhaps as much as an hour depending on your experience, to complete this page. If you run into issues, check out Troubleshoot SSH Issues for extra information that may help you along. You can even skip this whole page and continue to use HTTPS if you want.', '2014-10-25 18:33:22'),
(14, 'Step 1. Read a quick overview of SSH concepts', '<p>\r\n	Step 1. Read a quick overview of SSH concepts To use SSH with Bitbucket, you create an SSH identity. An identity consists of a private and a public key which together are a key pair. The private key resides on your local computer and the public you upload to your Bitbucket account. Once you upload a public key to your account, you can use SSH to connect with repositories you own and repositories owned by others, provided those other owners give your account permissions. By setting up SSH between your local system and the Bitbucket server, your system uses the key pair to automate authentication; you won&#39;t need to enter your password each time you interact with your Bitbucket repository. There are a few important concepts you need when working with SSH identities and Bitbucket You cannot reuse an identity&#39;s public key across accounts. If you have multiple Bitbucket accounts, you must create multiple identities and upload their corresponding public keys to each individual account. You can associate multiple identities with a Bitbucket account. You would create multiple identities for the same account if, for example, you access a repository from a work computer and a home computer. You might create multiple identities if you wanted to execute DVCS actions on a repository with a script &ndash; the script would use a public key with an empty passphrase allowing it to run without human intervention. RSA (R. Rivest, A. Shamir, L. Adleman are the originators) and digital signature algorithm (DSA) are key encryption algorithms. Bitbucket supports both types of algorithms. You should create identities using whichever encryption method is most comfortable and available to you.</p>\r\n', '2014-12-09 06:34:19'),
(15, 'Step 2. Check if you have existing default Identity', '<p>\r\n	Step 2. Check if you have existing default Identity The Git Bash shell comes with an SSH client. Do the following to verify your installation: Double-click the Git Bash icon to start a terminal session. Enter the following command to verify the SSH client is available: manthony@MANTHONY-PC ~ $ ssh -v OpenSSH_4.6p1, OpenSSL 0.9.8e 23 Feb 2007 usage: ssh [-1246AaCfgkMNnqsTtVvXxY] [-b bind_address] [-c cipher_spec] [-D [bind_address:]port] [-e escape_char] [-F configfile] [-i identity_file] [-L [bind_address:]port:host:hostport] [-l login_name] [-m mac_spec] [-O ctl_cmd] [-o option] [-p port] [-R [bind_address:]port:host:hostport] [-S ctl_path] [-w local_tun[:remote_tun]] [user@]hostname [command] If you have ssh installed, go to the next step. If you don&#39;t have ssh installed, install it now with your package manager. List the contents of your ~/.ssh directory. If you have not used SSH on Bash you might see something like this: manthony@MANTHONY-PC ~ $ ls -a ~/.ssh ls: /c/Users/manthony/.ssh: No such file or directory If you have a default identity already, you&#39;ll see two id_* files: manthony@MANTHONY-PC ~ $ ls -a ~/.ssh . .. id_rsa id_rsa.pub known_hosts In this case, the default identity used RSA encryption (id_rsa.pub). If you want to use an existing default identity for your Bitbucket account, skip the next section and go to create a config file.</p>\r\n', '2014-09-09 06:40:19'),
(16, 'Step 3. Set up your default identity', '<p>\r\n	Step 3. Set up your default identity By default, the system adds keys for all identities to the /Users/yourname/.ssh directory. The following procedure creates a default identity. Open a terminal in your local system. Enter ssh-keygen at the command line. The command prompts you for a file to save the key in: manthony@PHOENIX ~ $ ssh-keygen Generating public/private rsa key pair. Enter file in which to save the key (/c/Documents and Settings/manthony/.ssh/id_ rsa): Press enter to accept the default key and path, /c/Documents and Settings/manthony/.ssh/id_rsa, or you can create a key with another name. To create a key with a name other than the default, specify the full path to the key. For example, to create a key called my-new-ssh-key, you would enter a path like this at the prompt: manthony@PHOENIX ~ $ ssh-keygen Generating public/private rsa key pair. Enter file in which to save the key (/c/Documents and Settings/manthony/.ssh/id_ rsa): /c/Documents and Settings/manthony/My Documents/keys/my-new-ssh-key Enter and renter a passphrase when prompted. Unless you need a key for a process such as script, you should always provide a passphrase. The command creates your default identity with its public and private keys. The whole interaction looks similar to the following: manthony@MANTHONY-PC ~ $ ssh-keygen Generating public/private rsa key pair. Enter file in which to save the key (/c/Users/manthony/.ssh/id_rsa): Created directory &#39;/c/Users/manthony/.ssh&#39;. Enter passphrase (empty for no passphrase): Enter same passphrase again: Your identification has been saved in /c/Users/manthony/.ssh/id_rsa. Your public key has been saved in /c/Users/manthony/.ssh/id_rsa.pub. The key fingerprint is: e7:94:d1:a3:02:ee:38:6e:a4:5e:26:a3:a9:f4:95:d4 manthony@MANTHONY-PC manthony@MANTHONY-PC ~ $ List the contents of ~/.ssh to view the key files. You should see something like the following: $ ls ~/.ssh id_rsa id_rsa.pub The command created two files, one for the public key ( for example id_rsa.pub ) and one for the private key (for example, id_rsa ).</p>\r\n', '2014-11-29 06:43:00'),
(17, 'Step 4. Create a SSH config file', 'Step 4. Create a SSH config file\r\n\r\n    Using your favorite text editor, edit an existing (or create a new) ~/.ssh/config file.\r\n\r\n    Add an entry to the configuration file using the following format:\r\n\r\n    Host bitbucket.org\r\n     IdentityFile ~/.ssh/privatekeyfile\r\n\r\n    The second line is indented. That indentation (a single space) is important, so make sure you include it.  The second line is the location of your private key file.  If you are following along with these instructions, your file is here:\r\n\r\n     ~/.ssh/id_rsa\r\n\r\n    When you are done editing, your configuration looks similar to the following:\r\n\r\n    Host bitbucket.org\r\n     IdentityFile ~/.ssh/id_rsa\r\n\r\n    Save and close the file.\r\n    Restart the GitBash terminal.', '2014-10-25 18:44:23'),
(18, 'Step 5. Update your .bashrc profile file', 'Step 5. Update your .bashrc profile file\r\n\r\nIt is a good idea to configure your GitBash shell to automatically start the agent when launch the shell.  The .bashrc file is the shell initialization file. It contains commands that run each time your GitBash shell starts.  You can add commands to the .bashrc file that start the agent when you start GitBash. The folks at GitHub have developed a nice script for this (their script was developed from a post by Joseph M. Reagle Jr. from MIT on the cygwin list). To start the agent automatically, do the following.\r\n\r\n    Start GitBash.\r\n\r\n    Edit your ~/.bashrc file.\r\n    If you don''t have a .bashrc file you can create the file using your favorite text editor. Keep in mind the file must be in your ~ (home) directory and must be named exactly .bashrc.  \r\n\r\n    Add the following lines to the file:\r\n    Icon\r\n\r\n    Chrome and Opera introduce ASCII \\xa0 (non-breaking space characters) on paste that can appear in your destination file. If you copy and paste the lines below, copy from another browser to avoid this problem.\r\n    SSH_ENV=$HOME/.ssh/environment\r\n       \r\n    # start the ssh-agent\r\n    function start_agent {\r\n        echo "Initializing new SSH agent..."\r\n        # spawn ssh-agent\r\n        /usr/bin/ssh-agent | sed ''s/^echo/#echo/'' > "${SSH_ENV}"\r\n        echo succeeded\r\n        chmod 600 "${SSH_ENV}"\r\n        . "${SSH_ENV}" > /dev/null\r\n        /usr/bin/ssh-add\r\n    }\r\n       \r\n    if [ -f "${SSH_ENV}" ]; then\r\n         . "${SSH_ENV}" > /dev/null\r\n         ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {\r\n            start_agent;\r\n        }\r\n    else\r\n        start_agent;\r\n    fi\r\n    Save and close the file.\r\n    Close GitBash.\r\n\r\n    Reopen GitBash.\r\n    The system prompts you for your passphrase:\r\n\r\n    Welcome to Git (version 1.7.8-preview20111206)\r\n    Run ''git help git'' to display the help index.\r\n    Run ''git help <command>'' to display help for specific commands.\r\n    Enter passphrase for /c/Documents and Settings/manthony/.ssh/id_rsa:\r\n\r\n    Enter your passphrase.\r\n    After accepting your passphrase, the system displays the command shell prompt. \r\n\r\n    Verify that the script identity added your identity successfully by querying the SSH agent:\r\n    $ ssh-add -l\r\n    2048 0f:37:21:af:1b:31:d5:cd:65:58:b2:68:4a:ba:a2:46 /Users/manthony/.ssh/id_rsa (RSA)\r\n\r\nAfter you install your public key to Bitbucket, having this script should prevent you from having to enter a password each time you push or pull a repository from Bitbucket.', '2014-10-25 18:45:05'),
(19, 'Step 6. Install the public key on your Bitbucket account', '<p>\r\n	Step 6. Install the public key on your Bitbucket account Open a browser and log into Bitbucket. Choose avatar &gt; Manage Account from the menu bar. The system displays the Account settings page. Click SSH keys. The SSH Keys page displays. It shows a list of any existing keys. Then, below that, a dialog for labeling and entering a new key. In your terminal window, cat the contents of the public key file. For example: cat ~/.ssh/id_rsa.pub Select and copy the key output in the clipboard. If you have problems with copy and paste, you can open the file directly with Notepad. Select the contents of the file (just avoid selecting the end-of-file character). Back in your browser, enter a Label for your new key, for example, Default public key. Paste the copied public key into the SSH Key field. Click the Add key button: The system adds the key to your account. Return to the terminal window and verify your configuration by entering the following command. ssh -T git@bitbucket.org The command message tells you which Bitbucket account can log in with that key. conq: logged in as tutorials. You can use git or hg to connect to Bitbucket. Shell access is disabled. Verify that the command returns your account name. Click if you got a permission denied (publickey) message. The command tests your connection to Bitbucket as a Git user. It first sees if your SSH Agent has an identity loaded. The command then checks if that private key matches a public key for an existing Bitbucket account. You might have either problem. To make sure your identity is loaded, enter the following command: ssh-add -l If the identity isn&#39;t loaded, check your work in Step 5 above. If it is loaded, try reinstalling your public key on your Bitbucket account. Still having troubles? You can try our Troubleshoot SSH Issues page or email support@bitbucket.org.</p>\r\n', '2014-11-13 06:46:04'),
(20, 'Step 7. Configure your repository to use the SSH protocol', 'Step 7. Configure your repository to use the SSH protocol\r\n\r\nThe URL you use for a repository depends on which protocol you are using, HTTPS and SSH. The Bitbucket repository Overview page has a quick way for you to see the one for your bb101repo repo. On the repository''s Overview page look for the Clone this repository line. \r\n\r\nExperiment for a moment, click back and forth between the SSH and the HTTPS protocol links to see how the URLs differ. The table below shows the format for each DVCS based on protocol.\r\n\r\n \r\n	\r\nSSH URL format\r\n	\r\nHTTPS URL format\r\nMercurial	ssh://hg@bitbucket.org/accountname/reponame/	\r\n\r\nhttps://accountname@bitbucket.org/accountname/reponame\r\nGit	\r\n\r\ngit@bitbucket.org:accountname/reponame.git\r\n\r\nor\r\n\r\nssh://git@bitbucket.org/accountname/reponame.git\r\n	\r\n\r\nhttps://accountname@bitbucket.org/accountname/reponame.git\r\n\r\nIn the SSH format, the accountname appears after git@bitbucket.org or hg@bitbucket.org. In HTTPS format, the accountname before git@bitbucket.org or hg@bitbucket.org.\r\n\r\nGo to terminal on your local system and navigate to your bb101repo-practice repository. Then, do the following:\r\n\r\n    View your current repository configuration.\r\n    You should see something similar to the following:\r\n\r\n    manthony@MANTHONY-PC ~ \r\n    $ cat .git/config\r\n    [core]\r\n      repositoryformatversion = 0\r\n      filemode = true\r\n      bare = false\r\n      logallrefupdates = true\r\n      ignorecase = true\r\n    [remote "origin"]\r\n      fetch = +refs/heads/*:refs/remotes/origin/*\r\n      url = https://newuserme@bitbucket.org/newuserme/bb101repo.git\r\n    [branch "master"]\r\n      remote = origin\r\n      merge = refs/heads/master\r\n\r\n     \r\n\r\n    As you can see, the url is using the HTTPS protocol. There are a number of ways to change this value, the easiest way is just to edit the repository''s configuration file.\r\n    Edit the ~/repos/bb101repo-practice/.git/config file with your favorite editor.\r\n\r\n    Change the url value to use the SSH format for that repository.\r\n    When you are done the origin section should contain something similar to the following:\r\n\r\n    [remote "origin"]\r\n      fetch = +refs/heads/*:refs/remotes/origin/*\r\n      url = git@bitbucket.org:newuserme/bb101repo.git\r\n\r\n    Save your edits and close the file.', '2014-10-25 18:46:52'),
(21, 'Step 8. Make a change under the new protocol', '<p>\r\n	Step 8. Make a change under the new protocol Edit the README file in your bb101repo-practice repository. Add a new line to the file, for example: Welcome to My First Repo ------------------------------- This repo is a practice repo I am using to learn bitbucket. You can access this repo with SSH or with HTTPS. Save and close the file. Add and then commit your change to your local repo. git add README git commit -m &quot;making a change under the SSH protocol&quot; Push your changes. The system warns you that it is adding the Bitbucket host to the list of known hosts. manthony@MANTHONY-PC ~ $ git push $ git push Counting objects: 5, done. Delta compression using up to 2 threads. Compressing objects: 100% (2/2), done. Writing objects: 100% (3/3), 287 bytes, done. Total 3 (delta 1), reused 0 (delta 0) remote: bb/acl: newuserme is allowed. accepted payload. To git@bitbucket.org:newuserme/bb101repo.git 056c29c..205e9a8 master -&gt; master Open the repo Overview in Bitbucket to view your commit.</p>\r\n', '2014-09-25 06:47:21'),
(22, 'Next Steps', 'Next Steps\r\n\r\nNow, you should set up SSH for Mercurial through the TortoiseHg Workbench. If you are a Mac OSX or Linux user, you should have already worked through one page that covers SSH for both Git and Mercurial.', '2014-10-25 18:47:51'),
(23, 'Postcards from Hokkaido', '<p>\r\n	When I came to Hokkaido for the first time for a wildlife tour in 2013, I fell in love with the place. It&rsquo;s vast, wild, elegant, and her people live close to the land. I left with a longing to return, so when Martin Bailey asked me to come back and do this trip again, but with a landscape emphasis, I jumped on it and we started planning. 5 days ago a number of old friends, and a handful of new ones, met Martin and I in Tokyo, though Cynthia and I arrived a few days before that to spend some time with a couple close friends. We spend our days, short as they are (Sunrise around 8, sunset around 4) in driving snow, nearly gail-force winds, amazing land and seascapes, and the company of people who love being in wild places with cameras in hand. We spend the evenings in the onsens, Japanese natural hotsprings baths, and eating dinner and drinking sake (usually one after the other, though I&rsquo;m open to the idea of taking the sake into the baths!). It&rsquo;s a wonderful trip. You can see some of my work here, and a taste of the black and white images <a href="http://instagram.com/davidduchemin/" target="_blank">over on my Instagram page.&nbsp;</a></p>\r\n<p>\r\n	&nbsp;</p>\r\n', '2015-01-15 03:02:25'),
(24, 'Postcards from Venice', '<p>\r\n	<img alt="1" src="94e87-Mrkkleslikovnic.jpg" /></p>\r\n<p>\r\n	Venice is, at any time of year, magical. But I&rsquo;ve never enjoyed it this much, never found so much mood and beauty. The tourists are a little thinner now that it&rsquo;s cold out, but the light is gorgeous &ndash; this morning the sun just let the fog do its thing and didn&rsquo;t try to get in the way.</p>\r\n<p>\r\n	We&rsquo;ve got a beautiful little apartment here, not far from Teatro La Fenice, and in such a walkable city, everything seems close. I&rsquo;m looking out my window right now, gondolas going by with some regularity. Some of the images in this series are shot a couple feet from the sofa I&rsquo;m on now. I&rsquo;m not even going to pretend I&rsquo;m not deliriously happy about that.</p>\r\n<p>\r\n	Venice is, at any time of year, magical. But I&rsquo;ve never enjoyed it this much, never found so much mood and beauty. The tourists are a little thinner now that it&rsquo;s cold out, but the light is gorgeous &ndash; this morning the sun just let the fog do its thing and didn&rsquo;t try to get in the way.</p>\r\n<p>\r\n	We&rsquo;ve got a beautiful little apartment here, not far from Teatro La Fenice, and in such a walkable city, everything seems close. I&rsquo;m looking out my window right now, gondolas going by with some regularity. Some of the images in this series are shot a couple feet from the sofa I&rsquo;m on now. I&rsquo;m not even going to pretend I&rsquo;m not deliriously happy about that.</p>\r\n<p>\r\n	<img alt="2" src="00fb2-dramtreskaecMkukul.jpg" /></p>\r\n<p>\r\n	I&rsquo;ve felt greater creative freedom this time than I ever have in Venice. Perhaps I&rsquo;ve hit that point where I&rsquo;m not as worried about coming all this way and not &ldquo;getting the shot.&rdquo; Or maybe I&rsquo;m just having too much fun to worry about the fact that this new work is a bit of a departure from previous images in my Venice series. Despite the Leicas I adore and carry with me, it&rsquo;s the iPhone with its 18mm Moment lens on, and my Slow Shutter App that allows both longer exposures and multiple exposures, which I&rsquo;m combining for the images you see here.</p>\r\n<p>\r\n	In case I miss you in the coming days, thank you for another beautiful year. My very best to you and the ones you love over this beautiful holiday of love and light.</p>\r\n', '2015-01-15 07:36:28'),
(25, 'Favourite Things', '<p>\r\n	Ova e tekst na pocetok na postot, pred prvata slika.</p>\r\n<p>\r\n	<img alt="1" src="94e87-Mrkkleslikovnic.jpg" /></p>\r\n<p>\r\n	<img alt="2" src="923f5-toptdcule.jpg" /></p>\r\n<p>\r\n	<img alt="3" src="6a562-orasi.jpg" /></p>\r\n<p>\r\n	<img alt="4" src="4eb32-starivozW.jpg" /></p>\r\n<p>\r\n	Between you and me, sometimes the whole <em><strong>Gear is Good, Vision is Better</strong></em> thing makes me worry that people think I don&rsquo;t like toys, but if you know me at all, you know that&rsquo;s just crazy talk. I like stuff. I&rsquo;ve been accused of having champagne tastes, by no less an authority than my own mother.&nbsp; So this, not really one of those holiday gift guides &ndash; because it&rsquo;s probably too late anyways, is a short list of some of my favourite stuff from this year. In some way these 11 things just kind of made me a little happier. Once a year I open the floodgates on a discussion about gear and shiny things. Now, God help me, is that time. So, in the spirit of Sound of Music, but without having to suffer through listening to it again, here are a few of my favourite things&hellip;</p>\r\n<p>\r\n	<img alt="5" src="9fda1-osnezenitutnki.jpg" /></p>\r\n<p>\r\n	<img alt="6" src="d75be-belazimazutakuca.jpg" /></p>\r\n<p>\r\n	<a href="http://momentlens.co/" target="_blank"><strong>The Moment 18mm Lens for iPhone.</strong></a> I love my iPhone camera. And earlier this year I bought a slide-on case with a couple built-in lenses. I loved it, and though the quality took a back seat to convenience, it made me realize that the iPhone&rsquo;s camera just isn&rsquo;t wide enough for me. Enter Moment, a company that started with a successful Kickstarter last year and now creates two very nice, much higher quality, bayonet-mount lenses for mobile phones. I ordered the 18mm lens, but there&rsquo;s a slightly telephoto lens as well (no focal length given, they just say it&rsquo;s 2x closer). Beautifully machined, with big, glass optics, this is a really nice lens. Can&rsquo;t wait to use it in Venice next week. Moment lenses are just shy of $100.</p>\r\n<p>\r\n	<strong><a href="http://vsco.co/vscocam">VSCO Cam iPhone/Android App</a> </strong>The only app, other than Slow Shutter, that I really use for my mobile photography, VSCO Cam is excellent. It&rsquo;s got intuitive, simple controls, a great interface, and a wide range of film emulation effects that are pretty classy, if not a little trendy at times. VSCO Cam is free, with in-App purchasing of the filters and presets.</p>\r\n<p>\r\n	<a href="https://www.artifactuprising.com/" target="_blank"><strong>Artifact Uprising Prints.</strong></a> I&rsquo;ve talked about Artifact Uprising before, and as one of their ambassadors, who adores them and what they make,&nbsp; I&rsquo;ll keep talking about them. I&rsquo;m a big advocate of printing your work, and AU makes it easy to do so, and with really beautiful media. During 2015 I will be printing a set or two of prints after every trip with AU &ndash; I especially like the square print sets and the [Signature] Series.</p>\r\n<p>\r\n	<img alt="7" src="a12c9-zelenagstrica.jpg" /></p>\r\n<p>\r\n	<img alt="8" src="e37c1-babsirinc.jpg" /></p>\r\n', '2015-01-15 09:02:00'),
(26, 'Glory Days', '<p>\r\n	<img alt="1" src="00fb2-dramtreskaecMkukul.jpg" /></p>\r\n<p>\r\n	It might surprise some of you, and to others be a shocking reminder of how fast time flies, for me to tell you it was only 10 years ago I stepped off the stage as a comedian for the last time and returned to my dream of being a photographer. Comedy, which I did for twelve years and loved with all my being for most of that time, had grown wearying. The laughter of others never got tiring to me, but conjuring it up while playing a character, instead of being just me, without the stage name, without the funny faces, began to take its toll. And so I changed. On a dime, or so it seemed to those looking from the outside. In reality, none of us change so quickly, and I think the myth of re-inventing ourselves is just that. I think we change, and evolve, and the hard work is not in re-invention, it&rsquo;s in accepting that we&rsquo;re no longer the person we once were, and changing our lives &ndash; sometimes in truly difficult, dramatic ways &ndash; to remain aligned with that new person, a person we might not have noticed ourselves becoming.</p>\r\n<p>\r\n	<img alt="3" src="dbd4b-aerotreskaec.jpg" /></p>\r\n<p>\r\n	<img alt="4" src="dfb8c-zlatovrvoblci.jpg" /></p>\r\n<p>\r\n	It might surprise some of you, and to others be a shocking reminder of how fast time flies, for me to tell you it was only 10 years ago I stepped off the stage as a comedian for the last time and returned to my dream of being a photographer. Comedy, which I did for twelve years and loved with all my being for most of that time, had grown wearying. The laughter of others never got tiring to me, but conjuring it up while playing a character, instead of being just me, without the stage name, without the funny faces, began to take its toll. And so I changed. On a dime, or so it seemed to those looking from the outside. In reality, none of us change so quickly, and I think the myth of re-inventing ourselves is just that. I think we change, and evolve, and the hard work is not in re-invention, it&rsquo;s in accepting that we&rsquo;re no longer the person we once were, and changing our lives &ndash; sometimes in truly difficult, dramatic ways &ndash; to remain aligned with that new person, a person we might not have noticed ourselves becoming.</p>\r\n<p>\r\n	It might surprise some of you, and to others be a shocking reminder of how fast time flies, for me to tell you it was only 10 years ago I stepped off the stage as a comedian for the last time and returned to my dream of being a photographer. Comedy, which I did for twelve years and loved with all my being for most of that time, had grown wearying. The laughter of others never got tiring to me, but conjuring it up while playing a character, instead of being just me, without the stage name, without the funny faces, began to take its toll. And so I changed. On a dime, or so it seemed to those looking from the outside. In reality, none of us change so quickly, and I think the myth of re-inventing ourselves is just that. I think we change, and evolve, and the hard work is not in re-invention, it&rsquo;s in accepting that we&rsquo;re no longer the person we once were, and changing our lives &ndash; sometimes in truly difficult, dramatic ways &ndash; to remain aligned with that new person, a person we might not have noticed ourselves becoming.</p>\r\n<p>\r\n	<img alt="5" src="b2f71-zelcinnana.jpg" /></p>\r\n<p>\r\n	I left comedy to become a humanitarian photographer. I wanted to change the world; a one-week gig in Haiti enough to change my life forever. I remember being on a plane to a comedy gig in Texas &ndash; fly there, do the gig, sleep in crappy hotel, fly home &ndash; and this voice in my head asked me, What would you do if this were your last gig? I surprised myself, because my reaction wasn&rsquo;t panic, but relief. I knew right then that the gig, as they say, was up. It wasn&rsquo;t long after that that I went to Haiti and shot for a small NGO. And then Ethiopia with two friends on a self-funded trip to create a cookbook that never happened. I was younger then. I wore a khaki photo vest. I carried two large DSLRs and as many lenses as I could afford. I travel lighter now, both as a traveler and a human being.</p>\r\n<p>\r\n	<img alt="6" src="6a562-orasi.jpg" /></p>\r\n<p>\r\n	That gig led to others. I concentrated, because it&rsquo;s all I knew how to do, on creating good work and telling the stories I was living. And work came in. Assignments led to other assignments. I began to travel for up to a month at a time. I did whatever it took to fund the whole thing&nbsp; &ndash; sponsorships, teaching, small commercial gigs for local clients when I was home. But never once did anything seem certain. It always seemed so precarious. The hunger kept me moving. It fueled my best work. I hope it still does.</p>\r\n<p>\r\n	<img alt="7" src="9fda1-osnezenitutnki.jpg" /></p>\r\n<p style="text-align: justify;">\r\n	A lot of threads had to come together, many of them a lifetime in the making, for me to be where I was. I&rsquo;d been serious about photography since I was 14. I dabbled in art, learned to teach at a young age, traveled a lot, cut my marketing teeth trying to sell the world my comedy, did a lot of writing. I was into my thirties before the pieces came together, and even then it felt messy. Just a bunch of seemingly random threads tied together in a knot, forming a whole. But still just a bunch of threads. And somewhere along the way, life happened.</p>\r\n<p style="text-align: justify;">\r\n	And now&nbsp; almost 10 years later I&rsquo;m turning 43, there are books, and somehow I became a publisher, when I&rsquo;m not traveling and shooting. I&rsquo;ve become my own client for the most part. I&rsquo;m not even sure what to call myself anymore, and even less sure it matters. I still do humanitarian work, but also feel strongly pulled to conservation and environmental issues &ndash; after all, if we destroy the planet, the people go down on the same ship. Everywhere I&rsquo;ve photographed within the harshest humanitarian contexts, the environment has been brutalized. They are connected. I don&rsquo;t know where I&rsquo;m going, but my God, the ride is exciting.</p>\r\n<blockquote>\r\n	<p>\r\n		Whatever you do, don&rsquo;t stop moving forward. In art, in love, and in life. Don&rsquo;t lose the hunger. Long for the next chance to feed that hunger, not to silence it.</p>\r\n</blockquote>\r\n<p style="text-align: justify;">\r\n	I&rsquo;m not sure why, today, I&rsquo;m feeling so introspective / retrospective. My looming birthday, perhaps. But I think it&rsquo;s important to know &ndash; to remember, because we all know this, I think &ndash; that no one&rsquo;s life is a simple line from A to B. It&rsquo;s a wandering path, full of uncertainty, and it looks self-evident and easy only in hindsight. At the time none of us really know what the hell we&rsquo;re doing. But looking at it in hindsight, as though we&rsquo;ve arrived,&nbsp; can&rsquo;t be more than a momentary distraction, a place to rest and stretch our legs and our gratitude.</p>\r\n<p style="text-align: justify;">\r\n	Remember that <a href="http://www.lyricsdepot.com/bruce-springsteen/glory-days.html" target="_blank">Springsteen song, Glory Days?</a> Full of middle-aged people living their best lives in the past, it&rsquo;s as good a cautionary tale as you&rsquo;re going to get.&nbsp; We keep moving forward. Things remain uncertain, exciting. I keep changing, and so will you, which is &ndash; I think &ndash; the great hope. Everything changes. The plot will ever twist.&nbsp; Whatever you do, don&rsquo;t stop moving forward. In art, in love, and in life. Don&rsquo;t lose the hunger. Long for the next chance to feed that hunger, not to silence it. Do your best work, for the sake of doing your best work. Just don&rsquo;t start thinking you know remotely what that will look like. You won&rsquo;t. And hold the things you wish for with an open hand. The person you are in 10 years may not want any of them.</p>\r\n<p style="text-align: justify;">\r\n	<img alt="2" src="d75be-belazimazutakuca.jpg" /></p>\r\n', '2015-01-16 01:37:14');

-- --------------------------------------------------------

--
-- Table structure for table `post_category`
--

CREATE TABLE IF NOT EXISTS `post_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `post_category`
--

INSERT INTO `post_category` (`id`, `post_id`, `category_id`) VALUES
(1, 7, 1),
(2, 7, 5),
(3, 7, 8),
(4, 7, 11),
(5, 26, 2),
(27, 26, 6),
(28, 26, 9),
(29, 26, 1),
(30, 26, 11),
(31, 25, 1),
(32, 25, 2),
(33, 26, 8),
(34, 12, 6),
(35, 12, 7),
(36, 12, 12);

-- --------------------------------------------------------

--
-- Table structure for table `post_comments`
--

CREATE TABLE IF NOT EXISTS `post_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `commentator` varchar(250) NOT NULL,
  `commentator_email` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `post_comments`
--

INSERT INTO `post_comments` (`id`, `post_id`, `commentator`, `commentator_email`, `comment`, `created`) VALUES
(1, 26, 'leonie', '1', '<p>\r\n	i do love that we&rsquo;re in the same place and see totally different things. so happy to be travelling with you again, friend.</p>\r\n', '2015-01-15 12:02:01'),
(2, 26, 'Jerry Syder', '2', '<p>\r\n	Beautiful</p>\r\n', '2015-01-12 12:15:42'),
(3, 26, 'Tom Kostes', '3', '<p>\r\n	Great, cold, images&hellip;</p>\r\n<p>\r\n	The hot springs and sake sound wonderful!</p>\r\n', '2015-01-15 03:23:05'),
(4, 25, 'robin aka gotham girl', '4', '<p>\r\n	Oh my&hellip;to have wild places&hellip;great company&hellip;and everyone loves photography? Heaven!! Thanks for showing me a part of the world that is so amazing&hellip;love the instagram captures&hellip;and the colors in the image (third one from the bottom) is breathtaking!<br />\r\n	Thanks always for sharing!</p>\r\n', '2015-01-15 03:24:24'),
(5, 22, 'bobo', '100', 'koment', '0000-00-00 00:00:00'),
(6, 22, 'Lazarov', '100', 'EEEEeeeeee......', '0000-00-00 00:00:00'),
(7, 22, 'Kire Lazarov', '100', 'Ade, ade...', '2015-01-19 18:05:00'),
(8, 24, 'Hanz', '100', 'I mag sie sehr gern diese bild diese ort und Ich denke es ist irgendwie eine wundershon pATZ.', '2015-01-19 18:15:02'),
(9, 22, 'visitor visit', 'visitor@dvdpet.com.mk', 'Komentar od VISITOR USER', '2015-01-19 19:11:52'),
(10, 23, 'creator creator', 'creator@dvdpet.com.mk', 'Komentar od creator@dvdpet.com.mk', '2015-01-19 19:15:46'),
(11, 26, 'Zace', 'zace.ilio@gmail.com', 'Komentar od ne logiran korisnik', '2015-01-19 19:36:45'),
(12, 11, 'Karolina Goceva', 'k@k.c', 'Karolina Komentar', '2015-01-19 19:41:39'),
(13, 11, 'Bobo', 'bozo.max@hotmail.com', 'Krem biar luksuz komentarishen', '2015-01-19 19:43:05'),
(14, 11, 'Sarajevo', 'Sarajevo@Sarajevo.com', 'Sarajevo me nervira ko ce ostava pred grealica, aj dopigo ova likerov od visni, visnovka komentar', '2015-01-19 19:46:18'),
(15, 13, 'administrator', 'bozidar.ilio@gmail.com', 'Komentar od administratorot', '2015-01-19 23:06:39'),
(16, 13, 'Zace', 'afatistraf@boljerce.protocno.com', 'Da go vikni kiro i da go zacisti boljerceto, programi nameneti za celata publika', '2015-01-19 23:08:39'),
(17, 19, 'bobobit', 'bobo@bit.com', 'Bitbucket \r\n[Editor’s Note: This is the first of a series of guest blog posts around Bitbucket. This post is written by Will Salcido, CEO & Co-Founder of Bedrock Analytics Corporation. If you are interested in writing a guest post about your usage of Bitbucket, or best practices & tips, please contact us at guestposts@bitbucket.org, and we will reach out to you.]\r\n\r\nimage01\r\n\r\nWill Salcido is the CEO & Co-Founder of Bedrock Analytics Corporation. Will spent over a decade in the field of analytics across several leadership roles at Nestle, Novartis, Ghirardelli & Lindt. He developed analytical software for Ghirardelli that led him to solo-deploy the solution across 11 European countries in 2011. Bedrock Analytics is a data visualization and analytics software company based out of Oakland, CA.\r\n\r\nThe idea for what would become Bedrock Analytics came to me while I was working in Stockholm, Sweden. After nine international deployments of the analytical solution I had built for Ghirardelli & Lindt, I discovered that nearly all consumer product companies had similar issues working with data. Bedrock’s data visualization software enables consumer product companies to extract actionable insights from retail data. The software fills a skills gap for small to midsized companies and makes larger enterprises more efficient by automating the discovery of insights. Our customer base of consumer product companies spans throughout North America, Central America, and Europe.', '2015-01-21 17:29:47'),
(18, 10, 'visitor visit', 'visitor@dvdpet.com.mk', 'Bitka na aftomobili komentar', '2015-01-21 17:35:09'),
(19, 26, 'Jas', 'jas@bobo.com', '<p>\r\n	Ova e nov komentar za GroceryCRUD&nbsp;Using set_relation with callback_column problem (GroceryCRUD bug solved) :</p>\r\n<div>\r\n	&nbsp; &nbsp; private function _unique_field_name($field_name) {</div>\r\n<div>\r\n	&nbsp; &nbsp; &nbsp; &nbsp; //This s is because is better for a string to begin with a letter and not with a number</div>\r\n<div>\r\n	&nbsp; &nbsp; &nbsp; &nbsp; return &#39;s&#39;.substr(md5($field_name),0,8);&nbsp;</div>\r\n<div>\r\n	&nbsp; &nbsp; }</div>\r\n<div>\r\n	&nbsp; &nbsp; public function _column_short_post_title($value,$row) {</div>\r\n<div>\r\n	&nbsp; &nbsp; &nbsp; &nbsp; if (strlen($value)&gt;30) {</div>\r\n<div>\r\n	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $short_title = substr($value, 0, 15) . &#39; [...] &#39; . substr($value, -15);</div>\r\n<div>\r\n	&nbsp; &nbsp; &nbsp; &nbsp; } else {</div>\r\n<div>\r\n	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $short_title = $value;</div>\r\n<div>\r\n	&nbsp; &nbsp; &nbsp; &nbsp; }</div>\r\n<div>\r\n	&nbsp; &nbsp; &nbsp; &nbsp; return &#39;&lt;span title=&quot;&#39; . $value . &#39;&quot; &gt;&#39;.$short_title. &#39;&lt;/span&gt;&#39;;</div>\r\n<div>\r\n	&nbsp; &nbsp; }</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	<div>\r\n		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $crud = new grocery_CRUD();</div>\r\n	<div>\r\n		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</div>\r\n	<div>\r\n		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $crud-&gt;set_theme(&#39;flexigrid&#39;);</div>\r\n	<div>\r\n		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $crud-&gt;set_subject(&#39;Post Comment&#39;);</div>\r\n	<div>\r\n		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $crud-&gt;set_table(&#39;post_comments&#39;);</div>\r\n	<div>\r\n		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $crud-&gt;display_as(&#39;post_id&#39;,&#39;Post&#39;);</div>\r\n	<div>\r\n		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $crud-&gt;order_by(&#39;created&#39;,&#39;desc&#39;);</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $crud-&gt;callback_column($this-&gt;_unique_field_name(&#39;post_id&#39;),</div>\r\n	<div>\r\n		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; array($this,&#39;_column_short_post_title&#39;));</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $crud-&gt;set_relation(&#39;post_id&#39;,&#39;posts&#39;,&#39;title&#39;);</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $output = $crud-&gt;render();</div>\r\n	<div>\r\n		&nbsp;</div>\r\n	<div>\r\n		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $this-&gt;load-&gt;view(&#39;subview&#39;, $output);</div>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', '2015-01-24 09:45:41');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '2130706433', 'administrator', '$2y$08$zNlwgh9IXyUJnP/hiLNCf.DvcxDtNy94T3DU7XmpQT7qveK/B4I/S', '9462e8eee0', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1419190422, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(3, '::1', 'administrator', '$2y$08$vV4nNzm5JHnCVZCaxKsOu.fO593IiialOYNedwgjPFKqyU95F8FaW', '', 'bozidar.ilio@gmail.com', NULL, 'HnzRx3mXQXSD1XtyCDEPIe00933c08bc16a7c251', 1420454707, NULL, 1417712819, 1422122743, 1, 'Bozidar', 'Ilio', 'ADMIN', '076747912'),
(4, '::1', 'bobi ilio', '$2y$08$9EyBDeUC5Trn/xwVK5uzpuj.uRygPI1MKQUpEOm7.XMAOknnU6aTm', '', 'bozo.max@hotmail.com', NULL, NULL, NULL, NULL, 1419276998, NULL, 1, 'Bobi', 'Ilio', 'MOB', '076747912'),
(5, '::1', 'creator creator', '$2y$08$fY/BAd6oBLgEwwljZAQ/3.vjogbaDx0P.qLHf9xLR05/uk0u6k7ja', '', 'creator@dvdpet.com.mk', NULL, NULL, NULL, NULL, 1419277621, 1421694901, 1, 'Creator', 'creator', 'DVDPET', '000777'),
(6, '::1', 'visitor visit', '$2y$08$50x/a0rID3HWzded04OYsu.oxRt/4zzXdoqA96IbUviZMBHb9uLsq', '', 'visitor@dvdpet.com.mk', NULL, NULL, NULL, NULL, 1419282235, 1421931147, 1, 'Visitor', 'Visit', 'DVDPET', '000666325'),
(7, '::1', 'admindvdpet dvd5', '$2y$08$p7IbS8rOTbVpdX4WaMeISu9ZRSVqWmVR6aY9sbgE3nlj4B4tscRLS', '', 'admin@dvdpet.com.mk', NULL, NULL, NULL, NULL, 1419282525, NULL, 1, 'AdminDVDPET', 'dvd5', 'DVDPET', '000111'),
(8, '::1', 'forbidden forb', '$2y$08$mSTj4hkhyJwWA8f.cl.oD.69bT3AInGwhOywq7ReZrVXxJO8gjQ6C', '', 'forbidden@gmail.com', NULL, NULL, NULL, NULL, 1420455690, NULL, 1, 'Forbidden', 'forb', 'NO ACCESS', '666'),
(9, '::1', 'visitor forbidden visforb', '$2y$08$CYE2Zf7N28Vag2lNOAvFCeI5flTZLa3/NcYsDm3giQ8S81Fo8yE9q', '', 'vforb@yahoo.com', NULL, NULL, NULL, NULL, 1420456130, 1421709565, 1, 'Visitor Forbidden', 'visforb', 'MVR', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(7, 4, 3),
(14, 5, 3),
(15, 5, 4),
(18, 1, 1),
(19, 1, 4),
(23, 7, 1),
(26, 3, 1),
(32, 6, 2),
(33, 8, 3),
(38, 9, 2),
(39, 9, 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
