-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2015 at 02:10 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `prileppanorama_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('7a75e9c8a3c9a0ea52f69f5099d3357f', '::1', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36', 1420663027, 'a:9:{s:9:"user_data";s:0:"";s:17:"session_singleton";b:1;s:8:"identity";s:22:"bozidar.ilio@gmail.com";s:8:"username";s:13:"administrator";s:5:"email";s:22:"bozidar.ilio@gmail.com";s:7:"user_id";s:1:"3";s:14:"old_last_login";s:10:"1420589826";s:17:"flash:old:csrfkey";s:8:"1w3DOVAz";s:19:"flash:old:csrfvalue";s:20:"XCs97THg5Wae7CmbtzNz";}');

-- --------------------------------------------------------

--
-- Table structure for table `example_3`
--

CREATE TABLE IF NOT EXISTS `example_3` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `category` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `position` int(4) unsigned NOT NULL,
  `price` float NOT NULL,
  `qty` int(11) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `thumb` varchar(100) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `example_3`
--

INSERT INTO `example_3` (`id`, `name`, `title`, `category`, `description`, `position`, `price`, `qty`, `url`, `thumb`, `category_id`, `priority`) VALUES
(1, 'babsirinc', '', '', '', 0, 0, 0, 'ac926-babsirinc.jpg', 'thumb__ac926-babsirinc.jpg', 9, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `permission_level` int(11) NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `permission_level`, `description`) VALUES
(1, 'admin', 4, 'Administrator'),
(2, 'visitor', 1, 'Visitor'),
(3, 'member', 2, 'Member'),
(4, 'creator', 3, 'Creator'),
(5, 'forbidden', 0, 'Forbidden');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL,
  `slug` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `template` varchar(100) NOT NULL,
  `layout` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `contents` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `parent_id`, `slug`, `name`, `title`, `template`, `layout`, `position`, `visible`, `contents`) VALUES
(1, 0, 'home', 'Home', 'Home Page', 'custom', 'basic', 1, 1, 'This is contents of a Home Page.\r\n"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt"\r\n'),
(2, 0, 'about', 'About', 'About Us Page', 'custom', 'left_sidebar', 2, 1, '<p>\r\n	About Us Page contents</p>\r\n<p>\r\n	<b>It&#39;s one of the most</b> important elements on a company&#39;s website and also one of the most undervalued: the ubiquitous &quot;About Us&quot; page&mdash;that section on your site that has been collecting virtual dust because you haven&#39;t bothered to read it since, well, you first wrote it.<br />\r\n	<br />\r\n	You may not be paying it much attention, but visitors to your site are. And considering that your About Us page is where the world clicks to learn about your company and the services you offer, which can mean the potential loss or gain of a customer, it deserves a little more consideration and a lot more respect.</p>\r\n'),
(3, 0, 'contact', 'Contact', 'Contact Page', 'custom', 'right_sidebar', 5, 1, '<p>\r\n	Contact Page Contents</p>\r\n<p class="single-first-p">\r\n	The purpose of any website is to get people from your target audience interested in what you&rsquo;re offering. Whether it be a product or service, 9 times out of 10, someone is going to want to communicate with you further. Because of this, in almost any industry, you&rsquo;re going to want to create a contact page.</p>\r\n<p>\r\n	For some, this is that last page on the site map where you just throw a bunch of information. You can leave it up to the person to decide how they want to contact you and what they want to contact you about. For others, this is the last attempt to get your potential customer to give you their business.</p>\r\n<p>\r\n	The contact page is much more important than many give it credit. Many basic websites just throw some numbers and e-mails up and move along. But in most cases, this is the page your customer sees before they decide they want you on their project. Or before they decide they want to visit you to purchase your product.</p>\r\n'),
(4, 0, 'gallery', 'Gallery', 'Photo Gallery', 'custom', 'basic', 3, 1, ''),
(5, 0, 'blog', 'Blog', 'Blog Page', 'custom', 'basic', 4, 1, ''),
(6, 4, 'urban', 'Urban', 'Urpan Page Photos', 'photo_gallery', 'photo_gallery', 1, 1, ''),
(7, 4, 'fields', 'Fields', 'Fields Photo Gallery', 'photo_gallery', 'photo_gallery', 2, 1, ''),
(8, 4, 'animals', 'Animals', 'Animals Page Photo Gallery', 'photo_gallery', 'photo_gallery', 3, 1, ''),
(9, 4, 'toptash', 'Toptash', 'Toptash Page', 'photo_gallery', 'photo_gallery', 6, 1, ''),
(10, 4, 'satorovkamen', 'Satorov Kamen', 'Satorov Kamen Page', 'photo_gallery', 'photo_gallery', 7, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `position` int(4) unsigned NOT NULL,
  `price` float NOT NULL,
  `qty` int(11) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `thumb` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `name`, `title`, `category_id`, `description`, `position`, `price`, `qty`, `url`, `thumb`) VALUES
(1, 'starivozW', 'Urban', 1, '', 1, 0, 0, '8a907-starivozW.jpg', 'thumb__8a907-starivozW.jpg'),
(2, 'zelenagstrica', 'Animals', 1, '', 2, 0, 0, 'f2e0f-zelenagstrica.jpg', 'thumb__f2e0f-zelenagstrica.jpg'),
(4, 'lozjastrfkmnsky', 'Fields', 1, '', 3, 0, 0, '39518-lozjastrfkmnsky.jpg', 'thumb__39518-lozjastrfkmnsky.jpg'),
(5, 'dramtreskaecMkukul', 'Golden Peak', 1, '', 4, 0, 0, 'bd2a7-dramtreskaecMkukul.jpg', 'thumb__bd2a7-dramtreskaecMkukul.jpg'),
(6, 'lordMrkkleslikovnic', 'Markovi Kuli', 1, '', 5, 0, 0, '3100a-lordMrkkleslikovnic.jpg', 'thumb__3100a-lordMrkkleslikovnic.jpg'),
(7, 'PPshtrfkmnBw', 'Shatorovkamen', 1, '', 6, 0, 0, '2cd29-PPshtrfkmnBw.jpg', 'thumb__2cd29-PPshtrfkmnBw.jpg'),
(8, 'strfsunsetoWide', 'Sunset', 1, '', 7, 0, 0, '72cfd-strfsunsetoWide.jpg', 'thumb__72cfd-strfsunsetoWide.jpg'),
(9, 'dreamon', 'Toptash', 1, '', 8, 0, 0, '37ef3-dreamon.jpg', 'thumb__37ef3-dreamon.jpg'),
(11, 'babsirinc', 'Baba', 6, '', 2, 0, 0, 'e37c1-babsirinc.jpg', 'thumb__e37c1-babsirinc.jpg'),
(12, 'branaribro', 'Brana', 6, '', 3, 0, 0, '2676c-branaribro.jpg', 'thumb__2676c-branaribro.jpg'),
(13, 'odrekcno', 'Prilepska reka', 6, '', 4, 0, 0, '3e0aa-odrekcno.jpg', 'thumb__3e0aa-odrekcno.jpg'),
(14, 'starivozW', 'Voz', 6, '', 1, 0, 0, '4eb32-starivozW.jpg', 'thumb__4eb32-starivozW.jpg'),
(15, 'belazimazutakuca', 'Belo nebo', 7, '', 0, 0, 0, 'd75be-belazimazutakuca.jpg', 'thumb__d75be-belazimazutakuca.jpg'),
(16, 'lozjastrfkmnsky', 'Lozje', 7, '', 0, 0, 0, '46b08-lozjastrfkmnsky.jpg', 'thumb__46b08-lozjastrfkmnsky.jpg'),
(17, 'orasi', 'Orev', 7, '', 0, 0, 0, '6a562-orasi.jpg', 'thumb__6a562-orasi.jpg'),
(18, 'osnezenitutnki', 'Tutun sneg', 7, '', 0, 0, 0, '9fda1-osnezenitutnki.jpg', 'thumb__9fda1-osnezenitutnki.jpg'),
(19, 'zitkiztinadpt', 'Zito', 7, '', 0, 0, 0, '9a984-zitkiztinadpt.jpg', 'thumb__9a984-zitkiztinadpt.jpg'),
(20, 'koncina', 'Konjcinja', 8, '', 0, 0, 0, 'b9c15-koncina.jpg', 'thumb__b9c15-koncina.jpg'),
(21, 'insekc', 'Kolibri', 8, '', 0, 0, 0, '5e7e5-insekc.jpg', 'thumb__5e7e5-insekc.jpg'),
(22, 'ovcibravostrfkmn', 'Ovci', 8, '', 0, 0, 0, '6c8d4-ovcibravostrfkmn.jpg', 'thumb__6c8d4-ovcibravostrfkmn.jpg'),
(23, 'spidermealinvrs', 'Pajak', 8, '', 0, 0, 0, '9a352-spidermealinvrs.jpg', 'thumb__9a352-spidermealinvrs.jpg'),
(24, 'patceovcina', 'Pateka od ovci', 8, '', 0, 0, 0, 'e62c5-patceovcina.jpg', 'thumb__e62c5-patceovcina.jpg'),
(25, 'zelenagstrica', 'Guster zelen', 8, '', 0, 0, 0, 'a12c9-zelenagstrica.jpg', 'thumb__a12c9-zelenagstrica.jpg'),
(26, 'dreamon', 'Frame', 9, '', 0, 0, 0, 'beaed-dreamon.jpg', 'thumb__beaed-dreamon.jpg'),
(27, 'toptdcule', 'Presek', 9, '', 0, 0, 0, '923f5-toptdcule.jpg', 'thumb__923f5-toptdcule.jpg'),
(28, 'dreamzracina', 'Pelagonija', 9, '', 0, 0, 0, '93388-dreamzracina.jpg', 'thumb__93388-dreamzracina.jpg'),
(29, 'DSC_0173', 'Zalez', 9, '', 0, 0, 0, '649c6-DSC_0173.JPG', 'thumb__649c6-DSC_0173.JPG'),
(30, 'idilicno', 'Diagonala', 10, '', 0, 0, 0, '8edde-idilicno.jpg', 'thumb__8edde-idilicno.jpg'),
(31, 'karpsatorofkmen', 'Karpa Satorovkamen', 10, '', 0, 0, 0, '3fab0-karpsatorofkmen.jpg', 'thumb__3fab0-karpsatorofkmen.jpg'),
(32, 'krstecesma', 'Cesma Krste Parapanski', 10, '', 0, 0, 0, 'bcef2-krstecesma.jpg', 'thumb__bcef2-krstecesma.jpg'),
(33, 'zitkiztinadpt', 'Zito', 10, '', 0, 0, 0, '0183a-zitkiztinadpt.jpg', 'thumb__0183a-zitkiztinadpt.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `photo_categories`
--

CREATE TABLE IF NOT EXISTS `photo_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `photo_categories`
--

INSERT INTO `photo_categories` (`id`, `name`) VALUES
(1, 'urban'),
(2, 'animals');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '2130706433', 'administrator', '$2y$08$zNlwgh9IXyUJnP/hiLNCf.DvcxDtNy94T3DU7XmpQT7qveK/B4I/S', '9462e8eee0', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1419190422, 1, 'Admin', 'istrator', 'ADMIN', '0'),
(3, '::1', 'administrator', '$2y$08$vV4nNzm5JHnCVZCaxKsOu.fO593IiialOYNedwgjPFKqyU95F8FaW', '', 'bozidar.ilio@gmail.com', NULL, 'HnzRx3mXQXSD1XtyCDEPIe00933c08bc16a7c251', 1420454707, NULL, 1417712819, 1420623196, 1, 'Bozidar', 'Ilio', 'ADMIN', '076747912'),
(4, '::1', 'bobi ilio', '$2y$08$9EyBDeUC5Trn/xwVK5uzpuj.uRygPI1MKQUpEOm7.XMAOknnU6aTm', '', 'bozo.max@hotmail.com', NULL, NULL, NULL, NULL, 1419276998, NULL, 1, 'Bobi', 'Ilio', 'MOB', '076747912'),
(5, '::1', 'creator creator', '$2y$08$fY/BAd6oBLgEwwljZAQ/3.vjogbaDx0P.qLHf9xLR05/uk0u6k7ja', '', 'creator@dvdpet.com.mk', NULL, NULL, NULL, NULL, 1419277621, NULL, 1, 'Creator', 'creator', 'DVDPET', '000777'),
(6, '::1', 'visitor visit', '$2y$08$50x/a0rID3HWzded04OYsu.oxRt/4zzXdoqA96IbUviZMBHb9uLsq', '', 'visitor@dvdpet.com.mk', NULL, NULL, NULL, NULL, 1419282235, 1420472058, 1, 'Visitor', 'Visit', 'DVDPET', '000666325'),
(7, '::1', 'admindvdpet dvd5', '$2y$08$p7IbS8rOTbVpdX4WaMeISu9ZRSVqWmVR6aY9sbgE3nlj4B4tscRLS', '', 'admin@dvdpet.com.mk', NULL, NULL, NULL, NULL, 1419282525, NULL, 1, 'AdminDVDPET', 'dvd5', 'DVDPET', '000111'),
(8, '::1', 'forbidden forb', '$2y$08$mSTj4hkhyJwWA8f.cl.oD.69bT3AInGwhOywq7ReZrVXxJO8gjQ6C', '', 'forbidden@gmail.com', NULL, NULL, NULL, NULL, 1420455690, NULL, 1, 'Forbidden', 'forb', 'NO ACCESS', '666'),
(9, '::1', 'visitor forbidden visforb', '$2y$08$CYE2Zf7N28Vag2lNOAvFCeI5flTZLa3/NcYsDm3giQ8S81Fo8yE9q', '', 'vforb@yahoo.com', NULL, NULL, NULL, NULL, 1420456130, 1420472106, 1, 'Visitor Forbidden', 'visforb', 'MVR', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(7, 4, 3),
(14, 5, 3),
(15, 5, 4),
(18, 1, 1),
(19, 1, 4),
(23, 7, 1),
(26, 3, 1),
(32, 6, 2),
(33, 8, 3),
(38, 9, 2),
(39, 9, 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
